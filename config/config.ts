import { defineConfig } from 'umi';
import routes from './routes';
import proxy from './proxy';
// const { REACT_APP_ENV } = process.env;
export default defineConfig({
  routes: routes,
  layout: {
    siderWidth: 200,
    // collapsed: false,
    // breakpoint: false,
    // name: '管理后台',
    // logo: 'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
  },
  proxy: proxy['dev'],
});
