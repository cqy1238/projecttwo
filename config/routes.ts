export default [
  {
    exact: true,
    name: '登入',
    path: '/login',
    component: '@/pages/login',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    exact: true,
    name: '注册',
    path: '/register',
    component: '@/pages/register',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    exact: true,
    path: '/ArticleManager/allaticle/editor',
    component: '@/pages/ArticleManager/editor',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    exact: true,
    path: '/',
    name: '工作台',
    icon: 'dashboard',
    component: '@/pages/Workbench',
    wrappers: ['@/wrappers/auth'],
  },
  {
    path: '/ArticleManager',
    name: '文章管理',
    icon: 'form',
    routes: [
      {
        path: '/ArticleManager/allaticle',
        name: '所有文章',
        component: '@/pages/ArticleManager/allAticle',
        icon: 'Form',
      },
      {
        path: '/ArticleManager/classify',
        name: '分类管理',
        component: '@/pages/ArticleManager/classifyManager',
        icon: 'copy',
      },
      {
        path: '/ArticleManager/tags',
        name: '标签管理',
        component: '@/pages/ArticleManager/tagsManager',
        icon: 'tag',
      },
    ],
  },
  {
    path: '/PagesManager',
    name: '页面管理',
    icon: 'snippets',
    component: '@/pages/PagesManager',
  },
  {
    path: '/Knowledge',
    name: '知识小册',
    icon: 'book',
    component: '@/pages/Knowledge',
  },
  {
    path: '/PostersManager',
    name: '海报管理',
    icon: 'star',
    component: '@/pages/PostersManager',
  },
  {
    path: '/CommentsManager',
    name: '评论管理',
    icon: 'message',
    component: '@/pages/CommentsManager',
  },
  {
    path: '/EmailManager',
    name: '邮件管理',
    icon: 'mail',
    component: '@/pages/EmailManager',
  },
  {
    path: '/FileManager',
    name: '文件管理',
    icon: 'folderOpen',
    component: '@/pages/FileManager',
  },
  {
    path: '/SearchRecords',
    name: '搜索记录',
    icon: 'search',
    component: '@/pages/SearchRecords',
  },
  {
    path: '/AccessStatistics',
    name: '访问记录',
    icon: 'project',
    component: '@/pages/AccessStatistics',
  },
  {
    path: '/UserManager',
    name: '用户管理',
    icon: 'user',
    component: '@/pages/UserManager',
    access: 'canLogin',
  },
  {
    path: '/SystemSettings',
    name: '系统设置',
    icon: 'setting',
    component: '@/pages/SystemSettings',
    access: 'canLogin',
  },
  {
    path: '/ownspace',
    title: '个人中心',
    component: '@/pages/Ownspace',
    access: 'canLogin',
  },
];
