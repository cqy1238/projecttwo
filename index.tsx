import React, { useState, useRef, useEffect } from 'react'
import MDEditor, { commands } from '@uiw/react-md-editor';
import { CloseOutlined } from '@ant-design/icons';
import style from "./index.less"
import Articletext from "./Detailltext"
import { Upload, Tooltip, message, Button, Form, Input } from "antd"
import { Ossupload } from "@/api/user"
type Props = {}
interface IMenuData {
    title: string;
    type: string;
    level: number;
    key: number
}
export default function index({ }: Props) {
    const [text, setText] = useState(Articletext);
    const [menuData, setMenuData] = useState<IMenuData[]>([]);
    const perview = useRef<any>();
    const forMatterMenuData = () => {
        const previewEl = perview.current.querySelector('.w-md-editor-preview');
        const menuDataEl = Array.from(previewEl.querySelectorAll('*')).filter((item: any) => /^H[1-6]$/.test(item.nodeName));
        setMenuData(menuDataEl.map<IMenuData>((item: any, i) => ({
            title: item.innerText,
            type: item.nodeName,
            level: item.nodeName.slice(1) * 1,
            key: i
        })))
    }
    useEffect(() => {
        setTimeout(() => { // 异步任务
            if (perview.current && text) {
                forMatterMenuData(); // 重新计算侧边栏导航
            }
        }, 0)
    }, [perview, text])

    const hanleUpload = async (file: any) => { // 自定义上传行为
        const Data = new FormData();
        Data.append('file', file.file) // 表单格式发送
        const { data } = await Ossupload(Data)
        message.success('上传成功');
        setText((val) =>
            `${val}\n![${data.filename}](${data.url})`
        )
    }
    const hanleChange = (e: any) => {
        setText(e)
    }
    return <div className={style.Detail}>
        <div className={style.Detailhead}>
            <Button icon={<CloseOutlined />}></Button>
            <Input placeholder='请输入文章标题' bordered={false}></Input>
            <Button type="primary">发布</Button>
        </div>
        <div className={style.Detailmain}>
            <MDEditor
                value={text}
                ref={(val) => {
                    if (val) {
                        perview.current = val.container;
                    }
                }}
                onChange={hanleChange}
                commands={[  // 自定义toolbar
                    commands.title, //标题
                    commands.divider,
                    commands.bold,//加粗
                    commands.italic,//斜体
                    commands.quote,//引号
                    commands.strikethrough,//删除线
                    commands.orderedListCommand,//有序列表
                    {
                        name: 'test',
                        keyCommand: 'test',
                        icon: (
                            <Tooltip placement="top" title="上传图片" arrowPointAtCenter>
                                <Upload
                                    accept="image/png, image/jpeg, image/gif, image/jpg, image/svg"
                                    customRequest={hanleUpload}
                                    showUploadList={false}
                                >

                                    <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2464" width="13" height="13"><path d="M854.2208 110.7968 164.1472 110.7968c-72.4992 0-131.1744 58.7776-131.1744 131.1744l0 506.7776c0 72.4992 58.7776 131.1744 131.1744 131.1744l690.0736 0c72.4992 0 131.1744-58.7776 131.1744-131.1744L985.3952 241.9712C985.3952 169.5744 926.72 110.7968 854.2208 110.7968zM301.6704 278.9376c36.864 0 66.7648 29.9008 66.7648 66.7648s-29.9008 66.7648-66.7648 66.7648-66.7648-29.9008-66.7648-66.7648S264.8064 278.9376 301.6704 278.9376zM919.2448 638.1568c-8.6016 6.656-20.992 4.9152-27.5456-3.6864L744.5504 440.1152c-0.4096-0.6144-41.472-53.248-91.3408-48.2304-40.5504 4.608-80.384 47.8208-115.2 125.0304-36.864 82.0224-73.0112 124.8256-113.664 134.8608-46.08 11.1616-85.8112-20.0704-127.8976-53.3504l-5.3248-4.1984c-49.2544-38.6048-72.8064-29.7984-167.6288 63.0784-3.7888 3.7888-8.8064 5.632-13.7216 5.632-5.12 0-10.24-1.9456-14.0288-5.9392-7.5776-7.7824-7.4752-20.2752 0.3072-27.8528C182.1696 544.768 234.8032 500.0192 315.392 563.3024l5.4272 4.1984c35.9424 28.3648 66.9696 52.8384 94.208 46.08 27.136-6.656 56.4224-44.544 87.1424-112.7424 41.472-92.0576 90.8288-141.9264 146.944-148.0704 72.192-7.68 124.7232 60.7232 126.8736 63.6928l146.944 194.1504C929.4848 619.3152 927.8464 631.6032 919.2448 638.1568z" p-id="2465"></path></svg>
                                </Upload>
                            </Tooltip>

                        )
                    },

                ]}
            >
                <MDEditor.Markdown source={text} />
            </MDEditor>
            <div className={style.menu}>
                大纲
                {
                    menuData.map(val => (
                        <p key={val.key} style={{ marginLeft: val.level * 5 }}>
                            {val.title}
                        </p>
                    ))
                }
            </div>

        </div>
    </div>
}