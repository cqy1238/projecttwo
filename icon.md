工作台     workbench                <DashboardOutlined />
文章管理   Article Manager      <FormOutlined />
页面管理   page management      <SnippetsOutlined />
知识小册   Knowledge brochure     <BookOutlined />
海报管理   Posters management    <StarOutlined />
评论管理   Comments  management  <MessageOutlined />
邮件管理   email  management      <MailOutlined />
文件管理   file  management      <FolderOpenOutlined />
搜索记录   search  record          <SearchOutlined />
访问统计   Access statistics      <ProjectOutlined />
用户管理   user control           <UserOutlined />
系统设置   system  settings       <SettingOutlined />


所有文章   All articles            <EditOutlined />
分类管理   sort management          <CopyOutlined />
标签管理   label  management        <TagOutlined />
