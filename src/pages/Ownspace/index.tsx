import React from 'react';
import SystemView from '@/components/Ownspace/SystemView/index';
import PersonalData from '@/components/Ownspace/personalData/index';
import style from './index.less';

const Ownspace = () => {
  return (
    <div className={style.ownspace_wrap}>
      <SystemView />
      <PersonalData />
    </div>
  );
};
export default Ownspace;
