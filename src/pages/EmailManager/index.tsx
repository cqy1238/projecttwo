import BreadcrumbCom from '@/components/breadcrumb';
import { useState } from 'react';
import Form from '@/components/Email/EmainForm';
import Table from '@/components/Email/EmailTable';
import './index.less';

export default function EmailManager() {
  let [searchInfo, setSearchInfo] = useState({});
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form
          sendSearchInfo={(obj) => {
            setSearchInfo(obj);
          }}
        />
        <Table searchInfo={searchInfo} />
      </main>
    </div>
  );
}
