import BreadcrumbCom from '@/components/breadcrumb';
import { useEffect, useState, useCallback } from 'react';
import { Row, Col, Card, Form, Input, Button, message, Popconfirm } from 'antd';
import { useRequest } from 'umi';
import { categoryManager } from '@/services/article';
import style from './index.less';
interface CateType {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export default function Classify() {
  const [form] = Form.useForm();
  const [cate, setCate] = useState([]);
  const categoryApi = useRequest(categoryManager, {
    manual: true,
  });
  const [checkContext, setCon] = useState({ id: '1' });
  const fillForm = (item: any) => {
    form.setFieldsValue({
      label: item.label,
      value: item.value,
    });
    setCon(item);
  };
  const confirm = () => {
    categoryApi
      .run({
        method: 'delete',
        id: checkContext.id,
      })
      .then((res) => {
        init();
        message.success('删除分类成功');
        form.resetFields();
      });
  };
  const init = useCallback(() => {
    categoryApi
      .run({
        method: 'get',
      })
      .then((res) => {
        setCate(res);
      });
  }, []);
  useEffect(() => {
    init();
  }, []);
  const onFinish = (values: { label: string; value: string }) => {
    if (checkContext.id === '1') {
      //添加
      categoryApi
        .run({
          method: 'post',
          data: {
            ...values,
          },
        })
        .then((res) => {
          init();
          message.success('添加分类成功');
          form.resetFields();
        });
    } else {
      //更新
      categoryApi
        .run({
          method: 'patch',
          id: checkContext.id,
          data: {
            ...values,
          },
        })
        .then((res) => {
          init();
          message.success('更新分类成功');
          form.resetFields();
        });
    }
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Row gutter={1}>
          <Col span={10}>
            <Card
              title={checkContext.id === '1' ? '添加分类' : '管理分类'}
              style={{ width: 407, height: 267 }}
            >
              <Form onFinish={onFinish} form={form}>
                <Form.Item name="label">
                  <Input placeholder="请输入分类名称" />
                </Form.Item>
                <Form.Item name="value">
                  <Input placeholder="输入分类值（请输入英文，作为路由使用" />
                </Form.Item>
                <div className={style.btns}>
                  <Form.Item>
                    {checkContext.id === '1' ? (
                      <Button type="primary" htmlType="submit">
                        保存
                      </Button>
                    ) : (
                      <Button type="primary" htmlType="submit">
                        更新
                      </Button>
                    )}
                    {checkContext.id !== '1' && (
                      <Button
                        onClick={() => {
                          setCon({ id: '1' });
                          form.resetFields();
                        }}
                      >
                        返回添加
                      </Button>
                    )}
                  </Form.Item>
                  <Form.Item>
                    {checkContext.id !== '1' && (
                      <Popconfirm
                        title="确认删除这个分类?"
                        onConfirm={confirm}
                        okText="确认"
                        cancelText="取消"
                      >
                        <Button danger>删除</Button>
                      </Popconfirm>
                    )}
                  </Form.Item>
                </div>
              </Form>
            </Card>
          </Col>
          <Col span={10}>
            <Card title="所有分类" style={{ width: 610, height: 140 }}>
              <div className={style.classify}>
                {cate.map((item: CateType) => {
                  return (
                    <span
                      key={item.id}
                      onClick={() => {
                        fillForm(item);
                      }}
                    >
                      {item.label}
                    </span>
                  );
                })}
              </div>
            </Card>
          </Col>
        </Row>
      </main>
    </div>
  );
}
