import BreadcrumbCom from '@/components/breadcrumb';
import { useState, useEffect } from 'react';
import { useRequest, history } from 'umi';
import { articleManager, categoryManager } from '@/services/article';
import TablePanel from '@/components/tablePanel';
import {
  Tag,
  Space,
  Button,
  Popconfirm,
  Badge,
  message,
  Select,
  Row,
  Col,
  Form,
  Input,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import useList from '@/hooks/useList';
import moment from 'moment';
import style from './index.less';
const { Option } = Select;
interface DataType {
  [propsName: string]: any;
}
interface Iprops {
  [propsName: string]: any;
}
const ArticleManager = ({}: Iprops) => {
  //分类数据
  const cate = useRequest(categoryManager, { manual: true });
  const [cates, setCate] = useState([]);
  useEffect(() => {
    cate
      .run({
        method: 'get',
      })
      .then((res) => {
        setCate(res);
      });
  }, []);
  //请求页面接口
  const { run } = useRequest(articleManager, { manual: true });
  const { list, pagination, init, search } = useList(articleManager);
  //设置key值
  const [key, setkey] = useState<React.Key[]>([]);
  //表格头信息
  const columns: ColumnsType<DataType> = [
    {
      title: '标题',
      dataIndex: 'title',
      fixed: 'left',
      align: 'center',
      render: (title) => {
        return (
          <span
            className={style.point}
            onClick={() => {
              history.push('/ArticleManager/allaticle/editor');
            }}
          >
            {title}
          </span>
        );
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render: (text, record) => {
        return (
          <>
            {record?.status === 'publish' ? (
              <Badge status="success" text="已发布" />
            ) : (
              <Badge status="warning" text="草稿" />
            )}
          </>
        );
      },
    },
    {
      title: '分类',
      dataIndex: 'classify',
      align: 'center',
      render: (text, record) => {
        const styles = [
          'red',
          'volcano',
          'orange',
          'blue',
          '#87d068',
          '#108ee9',
        ];
        return (
          <Tag color={styles[Math.floor(Math.random() * styles.length)]}>
            {record.category?.label}
          </Tag>
        );
      },
    },
    {
      title: '标签',
      dataIndex: 'tag',
      align: 'center',
      render: (text, record) => {
        const styles = [
          'red',
          'volcano',
          'orange',
          'blue',
          '#87d068',
          '#108ee9',
        ];
        return record.tags.map((item: any) => {
          return (
            <Tag
              key={item?.id}
              color={styles[Math.floor(Math.random() * styles.length)]}
            >
              {item?.label}
            </Tag>
          );
        });
      },
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      align: 'center',
      render: (text, record) => {
        return (
          <Badge
            showZero
            count={record.views}
            style={{ backgroundColor: '#52c41a' }}
          />
        );
      },
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      align: 'center',
      render: (text, record) => {
        return (
          <Badge
            showZero
            count={record.likes}
            style={{ backgroundColor: 'rgb(235 47 151)' }}
          />
        );
      },
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      align: 'center',
      width: '200px',
      render: (timer) => {
        return <span>{moment(timer).format('YYYY-MM-DD, hh:mm:ss')}</span>;
      },
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '260px',
      render: (text, record) => (
        <Space size="middle">
          <a
            onClick={() => {
              toEditor(record);
            }}
          >
            编辑
          </a>
          <a
            onClick={() => {
              Focus(record);
            }}
          >
            {record.isRecommended ? '撤销首焦' : '首焦推荐'}
          </a>
          <a>查看访问</a>
          <Popconfirm
            title="确认删除这个文章?"
            onConfirm={() => {
              confirm(record.id);
            }}
            okText="确认"
            cancelText="取消"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const toEditor = (record: any) => {
    history.push({
      pathname: '/ArticleManager/allaticle/editor',
      query: {
        id: record.id,
      },
    });
  };
  //重新渲染视图
  const renderView = () => {
    message.success('操作成功');
    init({ page: pagination.page, pageSize: pagination.pageSize });
  };
  //首焦推荐/撤销
  const Focus = async (item: any) => {
    await run({
      method: 'PATCH',
      id: item.id,
      data: { isRecommended: !item.isRecommended },
    });
    renderView();
  };
  //获取keys方法
  const keys = (keys: string[]) => {
    setkey(keys);
  };
  //新建方法
  const newCon = () => {
    history.push('/ArticleManager/allaticle/editor');
  };
  //批量方法
  //批量发布
  const publishs = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { status: 'publish' },
      });
      renderView();
    });
  };
  //批量草稿
  const outlines = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { status: 'draft' },
      });
      renderView();
    });
  };
  //批量首焦推荐
  const allOnFocus = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { isRecommended: true },
      });
      renderView();
    });
  };
  //批量删除
  const confirms = () => {
    key.forEach(async (id) => {
      await run({
        method: 'DELETE',
        id,
      });
      renderView();
    });
  };
  //批量撤销推荐
  const allOutFocus = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { isRecommended: false },
      });
      renderView();
    });
  };

  //删除
  const confirm = async (id: string) => {
    await run({
      method: 'DELETE',
      id,
    });
    renderView();
  };
  //刷新
  const reload = () => {
    renderView();
  };
  //表单
  const [form] = Form.useForm();
  const reset = () => {
    form.resetFields();
  };
  const onFinish = async (values: any) => {
    const res = await run({
      method: 'get',
      params: {
        page: 1,
        pageSize: 5,
        ...values,
      },
    });
    search(res);
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          layout="inline"
        >
          <Row style={{ width: '100%', alignContent: 'center' }}>
            <Col span={7}>
              <Form.Item label="标题" name="title" style={{ width: '100%' }}>
                <Input placeholder="请输入文章标题" />
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item label="状态" name="status" style={{ width: '100%' }}>
                <Select placeholder="" allowClear>
                  <Option value="publish">已发布</Option>
                  <Option value="draft">草稿</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="分类" name="category">
                <Select placeholder="" allowClear>
                  {cates.map((item: any) => {
                    return (
                      <Option key={item.id} value={item.id}>
                        {item.label}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
            <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button htmlType="button" onClick={reset}>
                重置
              </Button>
            </Form.Item>
          </Row>
        </Form>
        <TablePanel
          newCon={newCon}
          keys={keys}
          columns={columns}
          data={list}
          reload={reload}
          pagination={pagination}
        >
          <Button style={{ marginRight: '8px' }} onClick={publishs}>
            发布
          </Button>
          <Button style={{ marginRight: '8px' }} onClick={outlines}>
            草稿
          </Button>
          <Button style={{ marginRight: '8px' }} onClick={allOnFocus}>
            首焦推荐
          </Button>
          <Button style={{ marginRight: '8px' }} onClick={allOutFocus}>
            撤销首焦
          </Button>
          <Popconfirm
            title="确认删除?"
            onConfirm={confirms}
            okText="确认"
            cancelText="取消"
          >
            <Button style={{ marginRight: '8px' }} danger>
              删除
            </Button>
          </Popconfirm>
        </TablePanel>
      </main>
    </div>
  );
};
export default ArticleManager;
