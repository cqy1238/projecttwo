import BreadcrumbCom from '@/components/breadcrumb';
import { Card, Form, Input, Button, Row, Col, Popconfirm, message } from 'antd';
import { useRequest } from 'umi';
import { useEffect, useState, useCallback } from 'react';
import { tagsManager } from '@/services/article';
import style from './index.less';
export default function Tag() {
  //后去表单
  const [form] = Form.useForm();
  //请求标签数据
  const [tags, setTags] = useState([]);
  const tagsApi = useRequest(tagsManager, {
    manual: true,
  });
  //选中的标签
  const [checkContext, setCon] = useState({ id: '1' });
  const fillForm = (item: any) => {
    form.setFieldsValue({ label: item.label, value: item.value });
    setCon(item);
  };

  const init = useCallback(() => {
    tagsApi
      .run({
        method: 'get',
      })
      .then((res) => {
        setTags(res);
      });
  }, []);
  useEffect(() => {
    init();
  }, [init]);
  //删除
  const confirm = () => {
    tagsApi
      .run({
        method: 'delete',
        id: checkContext.id,
      })
      .then((res) => {
        init();
        message.success('删除标签成功');
        form.resetFields();
      });
  };

  const onFinish = (values: { label: string; value: string }) => {
    if (checkContext.id === '1') {
      //添加
      tagsApi
        .run({
          method: 'post',
          data: {
            ...values,
          },
        })
        .then((res) => {
          init();
          message.success('添加标签成功');
          form.resetFields();
        });
    } else {
      //更新
      tagsApi
        .run({
          method: 'patch',
          id: checkContext.id,
          data: {
            ...values,
          },
        })
        .then((res) => {
          init();
          message.success('更新标签成功');
          form.resetFields();
        });
    }
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Row gutter={1}>
          <Col span={10}>
            <Card
              title={checkContext.id === '1' ? '添加标签' : '管理标签'}
              style={{ width: 407, height: 267 }}
            >
              <Form form={form} onFinish={onFinish}>
                <Form.Item name="label">
                  <Input placeholder="请输入标签名称" />
                </Form.Item>
                <Form.Item name="value">
                  <Input placeholder="输入标签值（请输入英文，作为路由使用" />
                </Form.Item>
                <div className={style.btns}>
                  {checkContext.id === '1' ? (
                    <Form.Item>
                      <Button type="primary" htmlType="submit">
                        保存
                      </Button>
                    </Form.Item>
                  ) : (
                    <>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          更新
                        </Button>
                        <Button
                          onClick={() => {
                            setCon({ id: '1' });
                            form.resetFields();
                          }}
                        >
                          返回添加
                        </Button>
                      </Form.Item>
                      <Form.Item>
                        <Popconfirm
                          title="确认删除这个标签?"
                          onConfirm={confirm}
                          okText="确认"
                          cancelText="取消"
                        >
                          <Button danger>删除</Button>
                        </Popconfirm>
                      </Form.Item>
                    </>
                  )}
                </div>
              </Form>
            </Card>
          </Col>
          <Col span={10}>
            <Card title="所有标签" style={{ width: 610 }}>
              <div className={style.tags}>
                {tags.map((item: any) => {
                  return (
                    <span
                      key={item.id}
                      onClick={() => {
                        fillForm(item);
                      }}
                    >
                      {item.label}
                    </span>
                  );
                })}
              </div>
            </Card>
          </Col>
        </Row>
      </main>
    </div>
  );
}
