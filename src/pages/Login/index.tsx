import React, { useEffect } from 'react';
import { Form, Input, Button, message } from 'antd';
import { useHistory, useModel } from 'umi';
import { login } from '@/services/user';
import loginSrc from './login.png';
import style from './index.less';
const Login: React.FC = () => {
  //清除本地存储
  useEffect(() => {
    localStorage.getItem('userinfo') && localStorage.removeItem('userinfo');
  }, []);
  const { setInitialState } = useModel('@@initialState');
  // const { data, run, error, loading } = useRequest(login, {
  //   manual: true,
  // });
  // const res = useRequest(login, {
  //   manual: true,
  // });

  const history = useHistory();
  const onFinish = async (values: any) => {
    let res = await login(values);
    console.log(res, 'res');

    if (res.statusCode === 200) {
      setInitialState(res);
      localStorage.setItem('userinfo', JSON.stringify(res.data));
      history.push('/');
    } else {
      message.error(res.msg);
    }
    // run(values)
    //   .then((res) => {
    //     console.log(res, 'res');
    //     if(res.statusCode && res.statusCode !== 200){
    //       message.error(res.msg);
    //     }
    //     setInitialState(res);
    //     localStorage.setItem('userinfo', JSON.stringify(res));
    //     history.push('/');

    //   })
    //   .catch((err) => { });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div className={style.login_box}>
      <div className={style.login_left}>
        <img src={loginSrc} alt="" />
      </div>
      <div className={style.login_right}>
        <div>
          <h2>系统登录</h2>
          <Form
            name="basic"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 25 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="账户"
              name="name"
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                登录
              </Button>
            </Form.Item>
            <Form.Item>
              <span
                onClick={() => {
                  history.push('/register');
                }}
              >
                Or 注册用户
              </span>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Login;
