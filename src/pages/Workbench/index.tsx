import React from 'react';
import style from './index.less';
import Header from '@/components/workbench/header/index';
import Penel from '@/components/workbench/panel/index';
import Fast from '@/components/workbench/fast/index';
import LatestArticles from '@/components/workbench/latestArticles/index';
import LatestComments from '@/components/workbench/latestComments/index';
type Props = {};

export default function Workbench() {
  return (
    <div className={style.Workbench_wrap}>
      <Header />
      <main>
        <Penel />
        <Fast />
        <LatestArticles />
        <LatestComments />
      </main>
    </div>
  );
}
