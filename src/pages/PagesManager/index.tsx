import BreadcrumbCom from '@/components/breadcrumb';
import { useState } from 'react';
import { useRequest } from 'umi';
import { getPages } from '@/services/pages';
import TablePanel from '@/components/tablePanel';
import {
  Space,
  Button,
  Popconfirm,
  Badge,
  message,
  Select,
  Row,
  Col,
  Form,
  Input,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import useList from '@/hooks/useList';
import moment from 'moment';
import './index.less';
const { Option } = Select;
interface DataType {
  [propsName: string]: any;
}
export default function PagesManager() {
  //请求页面接口
  const { run } = useRequest(getPages, { manual: true });
  const { list, pagination, init, search } = useList(getPages);
  console.log(pagination);

  //设置key值
  const [key, setkey] = useState<React.Key[]>([]);
  //表格头信息
  const columns: ColumnsType<DataType> = [
    {
      title: '名称',
      dataIndex: 'name',
      fixed: 'left',
      align: 'center',
    },
    {
      title: '路径',
      dataIndex: 'path',
      align: 'center',
    },
    {
      title: '顺序',
      dataIndex: 'order',
      align: 'center',
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      align: 'center',
      render: (count) => {
        return (
          <Badge
            showZero
            count={count}
            style={{ backgroundColor: '#52c41a' }}
          />
        );
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      render: (status) => {
        return status === 'publish' ? (
          <Badge status="success" text="已发布" />
        ) : (
          <Badge status="warning" text="草稿" />
        );
      },
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      align: 'center',
      render: (timer) => {
        return <span>{moment(timer).format('YYYY-MM-DD, hh:mm:ss')}</span>;
      },
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '260px',
      render: (text, record) => (
        <Space size="middle">
          <a>编辑</a>
          <a
            onClick={() => {
              publish(record);
            }}
          >
            {record.status === 'publish' ? '下线' : '发布'}
          </a>
          <a>查看访问</a>
          <Popconfirm
            title="确认删除?"
            okText="确认"
            cancelText="取消"
            onConfirm={() => confirm(record.id)}
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const renderView = () => {
    message.success('操作成功');
    init({ page: pagination.page, pageSize: pagination.pageSize });
  };
  //发布草稿
  const publish = async (item: any) => {
    await run({
      method: 'PATCH',
      id: item.id,
      data: { status: item.status === 'publish' ? 'draft' : 'publish' },
    });
    renderView();
  };
  //获取keys方法
  const keys = (keys: string[]) => {
    setkey(keys);
  };
  //新建方法
  const newCon = () => {
    console.log('新建');
  };
  //批量方法
  const publishs = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { status: 'publish' },
      });
      renderView();
    });
  };
  const outlines = () => {
    key.forEach(async (id) => {
      await run({
        method: 'PATCH',
        id,
        data: { status: 'draft' },
      });
      renderView();
    });
  };
  const confirm = async (id: string) => {
    await run({
      method: 'DELETE',
      id,
    });
    renderView();
  };
  const confirms = () => {
    key.forEach(async (id) => {
      await run({
        method: 'DELETE',
        id,
      });
      renderView();
    });
  };
  //刷新
  const reload = () => {
    renderView();
  };
  //表单
  const [form] = Form.useForm();
  const reset = () => {
    form.resetFields();
  };
  const onFinish = async (values: any) => {
    const res = await run({
      method: 'get',
      params: {
        page: 1,
        pageSize: 5,
        ...values,
      },
    });
    search(res);
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          layout="inline"
        >
          <Row style={{ width: '100%', alignContent: 'center' }}>
            <Col span={7}>
              <Form.Item label="名称" name="name" style={{ width: '100%' }}>
                <Input placeholder="请输入页面名称" />
              </Form.Item>
            </Col>
            <Col span={7}>
              <Form.Item label="路径" name="path" style={{ width: '100%' }}>
                <Input placeholder="请输入页面路径" />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="状态" name="status">
                <Select placeholder="" allowClear>
                  <Option value="publish">已发布</Option>
                  <Option value="draft">草稿</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
            <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button htmlType="button" onClick={reset}>
                重置
              </Button>
            </Form.Item>
          </Row>
        </Form>
        <TablePanel
          newCon={newCon}
          keys={keys}
          columns={columns}
          data={list}
          reload={reload}
          pagination={pagination}
        >
          <Button style={{ marginRight: '8px' }} onClick={() => publishs()}>
            发布
          </Button>
          <Button style={{ marginRight: '8px' }} onClick={() => outlines()}>
            下线
          </Button>
          <Popconfirm
            title="确认删除?"
            okText="确认"
            cancelText="取消"
            onConfirm={confirms}
          >
            <Button style={{ marginRight: '8px' }} danger>
              删除
            </Button>
          </Popconfirm>
        </TablePanel>
      </main>
    </div>
  );
}
