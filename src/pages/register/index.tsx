import React, { useState } from 'react';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { history } from 'umi';
import { register } from '@/services/user';
import registerSrc from './register.png';
import style from './index.less';
import RegisterModal from '@/components/registerModal/index';
const Register: React.FC = () => {
  const [flag, setFlag] = useState(false);
  const [res, setRes] = useState({});
  // const { run, loading } = useRequest(register, {
  //   manual: true,
  // });
  const onFinish = async (values: any) => {
    let res = await register(values);
    setRes(res.data);
    if (res.statusCode === 201) {
      setFlag(true);
    } else {
      message.error(res.msg);
    }
    // console.log('Success:', values);
    // run(values).then((res: any) => {
    //   console.log(res);
    //   // setFlag(true);
    // });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.login_box}>
      <div className={style.login_left}>
        <img src={registerSrc} alt="" />
      </div>
      <div className={style.login_right}>
        <div>
          <h2>访客注册</h2>
          <Form
            name="basic"
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 25 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="账户"
              name="name"
              rules={[{ required: true, message: '请输入用户名' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '请输入密码' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="确认"
              name="confirm"
              rules={[{ required: true, message: '请再次输入密码' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                注册
              </Button>
            </Form.Item>
            <Form.Item>
              <span
                onClick={() => {
                  history.push('/login');
                }}
              >
                Or 去登录
              </span>
            </Form.Item>
            {flag ? <RegisterModal /> : ''}
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Register;
