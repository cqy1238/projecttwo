import BreadcrumbCom from '@/components/breadcrumb';
import { Pagination } from 'antd';
import OSS from '../../components/OSS/index';
import FileForm from '../../components/file/FileForm';
import style from './index.less';
import { useEffect, useState } from 'react';
import FileCard from '@/components/File/FileCard';
import Drawer from '@/components/DrawerCom/DrawerCom';
import useRequest from '@ahooksjs/use-request';
import { getFileList } from '@/services/file';

type props = {
  type?: string;
};
interface searchType {
  name?: string;
  types?: string;
}

export default function FileManager(props: props) {
  const [fileItem, setFileItem] = useState({});
  window.localStorage.setItem('fileItem', JSON.stringify(fileItem));
  // 文件列表
  let [file, setFile] = useState([]);
  // 页码
  let [page, setPage] = useState(1);
  // 显示页数
  let [pageSize, setPageSize] = useState(12);
  // 总长度
  let [length, setLength] = useState(0);
  // 遮罩层开关
  let [mask, setMask] = useState(false);
  // 子组件传过来的遮罩层信息
  let [maskInfo, setMaskInfo] = useState({});
  // 搜索信息
  let [originname, setOriginname] = useState('');
  let [type, setType] = useState('');
  // 调用时的类型
  const [callType, setCallType] = useState('');
  // 使用useRequest调接口
  let apiFile = useRequest(getFileList, {
    // 手动获取接口
    manual: true,
  });
  // 获取文件列表
  const getFile = (
    page?: number,
    pageSize?: number,
    originalname?: string,
    type?: string,
  ) => {
    apiFile.run({ page, pageSize, originalname, type }).then((res) => {
      setFile(res.data[0]);
      setLength(res.data[1]);
    });
  };
  useEffect(() => {
    setCallType(String(props.type));

    return () => {
      setCallType('file');
    };
  }, []);
  // 获取分页数据
  useEffect(() => {
    getFile(page, pageSize, originname, type);
  }, [page, pageSize, originname, type]);
  return (
    <div className="allaticle">
      <header>{callType !== 'knowledge' ? <BreadcrumbCom /> : null}</header>
      <main>
        {callType !== 'knowledge' ? <OSS /> : null}
        <FileForm
          sendSearchInfo={(obj: searchType) => {
            let { name, types } = obj;
            setOriginname(String(name));
            setType(String(types));
          }}
        />
        <FileCard
          file={file}
          showInfo={(item) => {
            setFileItem(item);
            setMaskInfo(item);
            setMask(true);
          }}
          callType={callType}
          reload={() => getFile(page, pageSize, originname, type)}
        />
        <Pagination
          onChange={(e) => setPage(e)}
          className={style.pagiBox}
          total={length}
          showTotal={(total) => `共${total}条`}
          defaultPageSize={pageSize}
          pageSize={pageSize}
          defaultCurrent={1}
          current={page}
          pageSizeOptions={[8, 12, 24, 36]}
          onShowSizeChange={(current, size) => setPageSize(size)}
        />
        <Drawer
          flag={callType == 'knowledge' ? false : mask}
          state={maskInfo}
          changeFlag={(flag) => setMask(flag)}
          resetData={(flag) => {
            flag ? getFile() : null;
          }}
        />
      </main>
    </div>
  );
}
