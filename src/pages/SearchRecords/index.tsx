import BreadcrumbCom from '@/components/breadcrumb';
import Form from '@/components/Searchfrom';
import Table from '@/components/Searchtable';
import { useState } from 'react';
import style from './index.less';
interface obj {
  [name: string]: string;
}
export default function SearchRecords() {
  let [tablefrom, setfrom] = useState({});
  const serchfrom = (from: obj) => {
    console.log(from);
    setfrom(from);
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form serchfrom={serchfrom} />
        <Table tablefrom={tablefrom} />
      </main>
    </div>
  );
}
