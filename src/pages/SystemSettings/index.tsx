import BreadcrumbCom from '@/components/breadcrumb';
import Tabs from '@/components/SystemSettings/Tabs/index';
import './index.less';
export default function SystemSettings() {
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <div className="settable">
        <Tabs></Tabs>
        {/* <Form /> */}
        {/* <Table /> */}
      </div>
    </div>
  );
}
