import BreadcrumbCom from '@/components/breadcrumb';
import Form from '@/components/PostersManager/form';
import OSS from '@/components/OSS/index';
import { Empty } from 'antd';
import style from './index.less';
export default function PostersManager() {
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <OSS></OSS>
        <Form />
        <Empty
          className={style.empty}
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          style={{
            background: '#fff',
            height: '260px',
            paddingTop: '100px',
          }}
        />
      </main>
    </div>
  );
}
