import BreadcrumbCom from '@/components/breadcrumb';
import Table from '@/components/CommentsManager/Commentstable';
import './index.less';
export default function CommentsManager() {
  return (
    <div className="allaticle">
      <header>
        {/* 评论 */}
        <BreadcrumbCom />
      </header>
      <main>
        <Table />
      </main>
    </div>
  );
}
