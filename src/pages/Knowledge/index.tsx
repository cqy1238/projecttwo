import BreadcrumbCom from '@/components/breadcrumb';
import Card from '@/components/Knowledge/card';
import Form from '@/components/Knowledge/form';
import style from './index.less';

export default function knowledge() {
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form />
        <div className={style.card}>
          <Card></Card>
        </div>
      </main>
    </div>
  );
}
