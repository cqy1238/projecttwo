import BreadcrumbCom from '@/components/breadcrumb';
import Form from '@/components/Accessfrom';
import { useState } from 'react';
import Table from '@/components/Accesstable';
import './index.less';
export default function knowledge() {
  interface obj {
    [name: string]: string;
  }
  let [tablefrom, setfrom] = useState({});
  const serchfrom = (from: obj) => {
    console.log(from);
    setfrom(from);
  };
  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        <Form serchfrom={serchfrom} />
        <Table tablefrom={tablefrom} />
      </main>
    </div>
  );
}
