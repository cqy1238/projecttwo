import BreadcrumbCom from '@/components/breadcrumb';
import Userform from '@/components/UserManagement/form/index';
import Usertable from '@/components/UserManagement/table/index';
// import { UserManageList, Rbac } from '@/services/usermanager';
// import { useRequest } from 'umi';
// import { message } from 'antd';
import styles from './index.less';

export default function UserManager() {
  // const { data, error, loading } = useRequest(UserManageList);

  return (
    <div className="allaticle">
      <header>
        <BreadcrumbCom />
      </header>
      <main>
        {/* <Userform ></Userform> */}
        <Usertable></Usertable>
      </main>
    </div>
  );
}
