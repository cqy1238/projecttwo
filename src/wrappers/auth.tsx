import { Redirect } from 'umi';
const useAuth = () => {
  return localStorage.getItem('userinfo')
    ? { isLogin: true }
    : { isLogin: false };
};
export default (props: any) => {
  const { isLogin } = useAuth();
  if (isLogin) {
    return <div>{props.children}</div>;
  } else {
    return <Redirect to="/login" />;
  }
};
