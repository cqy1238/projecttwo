export const getId = () => {
  let id = '';
  const range = '123456789abcdefghijklmnopqrstuvwxyz';
  for (let i = 0; i < 32; i++) {
    id += range.substr(Math.floor(Math.random() * 0x10), 1);
  }
  return id;
};
