import { useState, useEffect, useCallback } from 'react';
const defaultPagination = {
  current: 1,
  pageSize: 5,
};
const usePage = (config: any = defaultPagination) => {
  const [Pagination, setPagination] = useState({
    current: 1,
    pageSize: 5,
  });
  const pagination = useCallback(() => {
    return {
      ...defaultPagination,
      ...config,
      current: Pagination.current,
      pageSize: Pagination.pageSize,
      onChange: (current: number, pageSize: number) => {
        if (config.onChange) {
          config.onChange(current, pageSize);
        }
        setPagination({ current, pageSize });
      },
    };
  }, [config, Pagination.current]);

  return pagination();
};

export default usePage;
