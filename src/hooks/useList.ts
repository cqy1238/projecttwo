import { useEffect, useCallback, useState } from 'react';
import usePage from '@/hooks/usePage';
import { useRequest } from 'umi';
const useList = (api: any) => {
  const [data, setDate] = useState({
    list: [],
    totle: 0,
  });
  //接口
  const { run } = useRequest(api, { manual: true });
  //初始化 页面
  const init = useCallback(async ({ page, pageSize }) => {
    const res = await run({
      method: 'get',
      params: { page, pageSize },
    });
    setDate({
      list: res[0],
      totle: res[1],
    });
  }, []);
  useEffect(() => {
    init({ page: 1, pageSize: 5 });
  }, []);
  //分页
  const pagination = usePage({
    total: data.totle,
    current: 1,
    pageSize: 5,
    showTotal: (total: any) => `共 ${total} 条`,
    pageSizeOptions: [5, 8, 12, 15],
    onChange: (page: number, pageSize: number) => {
      //发接口请求
      init({ page, pageSize });
    },
  });
  //接受搜索数据
  const search = (res: any) => {
    setDate({
      list: res[0],
      totle: res[1],
    });
  };
  return {
    list: data.list,
    pagination,
    init,
    search,
  };
};

export default useList;
