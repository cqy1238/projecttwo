export default function (initialState: any) {
  const { role } = initialState;
  return {
    canLogin: role === 'admin',
  };
}
