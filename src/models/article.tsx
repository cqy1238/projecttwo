import { getAllArticle } from '@/services/article';
const Article = {
  namespace: 'article',
  state: {
    articleList: [],
  },
  effects: {
    *getAllArticleData(
      { payload }: { payload: any },
      { call, put }: { call: any; put: any },
    ) {
      const { data } = yield call(getAllArticle, payload);
      yield put({
        type: 'setArticleList',
        payload: data,
      });
    },
  },
  reducers: {
    setArticleList(state: any, { payload }: { payload: any }) {
      return {
        ...state,
        articleList: [...payload[0]],
      };
    },
  },
};

export default Article;
