import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getAllArticle } from '@/services/article';
export interface WorkbenchModelState {
  articleList: any[];
}

export interface WorkbenchModelType {
  namespace: 'workbench';
  state: WorkbenchModelState;
  effects: {
    getArticleList: Effect;
  };
  reducers: {
    changeArticleList: Reducer<WorkbenchModelState>;
  };
}
const Workbench: WorkbenchModelType = {
  namespace: 'workbench',
  state: {
    articleList: [],
  },
  effects: {
    *getArticleList({ payload }: any, { call, put }: { call: any; put: any }) {
      let res = yield call(getAllArticle, payload);
      console.log(res, 'res');
      yield put({
        type: 'changeArticleList',
        payload: res.data,
      });
    },
  },
  reducers: {
    changeArticleList(state: any, action: any) {
      return {
        ...state,
        articleList: action.payload[0],
      };
    },
  },
};

export default Workbench;
