// import { getCommentList } from '@/services/comment';
// const Comment = {
//   namespace: 'comment',
//   state: {
//     commentList: [],
//   },
//   effects: {
//     *getCommentData(
//       { payload }: { payload: any },
//       { call, put }: { call: any; put: any },
//     ) {
//       let { data } = yield call(getCommentList, payload);
//       console.log(data);

//       yield put({
//         type: 'setCommentList',
//         payload: data,
//       });
//     },
//   },
//   reducers: {
//     setCommentList(state: any, { payload }: { payload: any }) {
//       return {
//         ...state,
//         ...payload,
//         commentList: payload[0],
//       };
//     },
//   },
// };

// export default Comment;
