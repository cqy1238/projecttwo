import { RequestConfig } from 'umi';
import './app.less';
import { Button, message, Dropdown, Menu } from 'antd';
//引入HeaderRight
import HeaderRight from '@/components/headerRight';
//引入footer
import Footer from '@/components/footer';
//引入面包屑
import BreadcrumbCom from './components/breadcrumb';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { createLogger } from 'redux-logger';
export const layout = () => {
  // let collapsed=true
  return {
    rightContentRender: () => <HeaderRight />,
    footerRender: () => <Footer />,
    onPageChange: () => {
      console.log('onPageChange');
    },
    onCollapse: (collapsed: boolean) => {
      console.log(collapsed);
    },
    // collapsedButtonRender: () => {
    //   return <div></div>;
    // },
    // headerContentRender: () => {
    //   return (
    //     <div
    //       // onClick={() => (collapsed= !collapsed)}
    //       style={{
    //         cursor: 'pointer',
    //         fontSize: '16px',
    //       }}
    //     >
    //       <MenuUnfoldOutlined />
    //     </div>
    //   );
    // },
    menuHeaderRender: () => {
      const menu = (
        <Menu
          onClick={(e) => {
            e.domEvent.stopPropagation();
          }}
          items={[
            {
              key: '1',
              label: <a href="/UserManager">新建文章-协同编辑器</a>,
            },
            {
              key: '2',
              label: <a href="/ArticleManager/allaticle/editor">新建文章</a>,
            },
            {
              key: '3',
              label: <a href="/SystemSettings">新建页面</a>,
            },
          ]}
        />
      );
      return (
        <div className="logo-container">
          <div>
            <div>
              <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png" />
            </div>
            <h2>管理平台</h2>
          </div>
          <Dropdown overlay={menu} placement="bottom">
            <Button type="primary"> + 新建</Button>
          </Dropdown>
        </div>
      );
    },
  };
};
const TOKEN_TYPE = 'Bearer';
export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    // 请求前拦截器  、检验身份
    (url: any, options: any) => {
      console.log('接口请求之前执行，处理请求接口时携带的公共参数');
      const headers = {
        ...options.headers,
      };
      const userInfo: any =
        localStorage.getItem('userinfo') &&
        JSON.parse(localStorage.getItem('userinfo')!);
      // console.log(userInfo.token);
      if (!options.isAuthorization && userInfo) {
        // 获取本地存储的token
        headers.authorization = `${TOKEN_TYPE} ${userInfo && userInfo.token}`;
      }
      return {
        url: `${url}`, // 请求地址
        options: {
          ...options,
          interceptors: true,
          headers,
        }, // 请求头，请求参数，请求方式
      };
    },
  ],
  responseInterceptors: [
    async (response: any) => {
      const res = await response.json();
      if (res.statusCode === 403) {
        res.msg = '访客无权进行该操作';
        message.warning(res.msg);
      }
      return res;
    },
  ],
};

export const getInitialState = () => {
  return localStorage.getItem('userinfo')
    ? JSON.parse(localStorage.getItem('userinfo')!)
    : { role: 'admin' };
};
export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
