import { request } from 'umi';
interface Params {
  name?: string;
  password?: string;
  confirm?: string;
  role?: string;
  oldPassword?: string;
  newPassword?: string;
  email?: string;
  avatar?: string;
  createAt?: string;
  status?: string;
  updateAt?: string;
}
interface RegisterItem {
  name: string;
  password: string;
}
// 登录接口
export const login = (data: Params) =>
  request('/api/auth/login', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
// 注册接口
export const register = (data: Params) =>
  request('/api/user/register', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
// 修改账号接口
export const getUpdate = (data: Params) =>
  request('/api/user/update', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
// 更新密码接口
export const getPassword = (data: Params) =>
  request('/api/user/password', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
