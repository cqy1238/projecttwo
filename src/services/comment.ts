import { request } from 'umi';
interface Params {
  [propsname: string]: any;
}
export const commentmanager = (params: Params) => {
  return request(`/api/comment/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};

//数据渲染接口
export const getCommentList = (params?: Params) => {
  return request('/api/comment', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
//通过拒绝接口
export const getPatchList = (params: Params) => {
  return request(`/api/comment/${params.id}`, {
    method: 'patch',
    data: { pass: params.pass },
    skipErrorHandler: true,
  });
};
//删除接口
export const getDeleteList = (params: Params) => {
  return request(`/api/comment/${params.id}`, {
    method: 'delete',
    skipErrorHandler: true,
  });
};
//回复接口
export const getReplyList = (params: Params) => {
  return request(`/api/comment`, {
    method: 'post',
    data: params,
    skipErrorHandler: true,
  });
};
