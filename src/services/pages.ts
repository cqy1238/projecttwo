import { request } from 'umi';
interface Params {
  [propsName: string]: any;
}

//获取数据   /api/page      GET
//发布      /api/page:id    PATCH   {status: "publish"}
//下线      /api/page:id    PATCH   {status: "draft"}
//删除      /api/page:id    DELETE

//获取页面
export const getPages = (params: Params) => {
  return request(`/api/page/${params.id ? params.id : ''}`, {
    method: params.method,
    params: params.params,
    data: params.data,
  });
};
