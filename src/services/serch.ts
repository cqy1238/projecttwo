import { request } from 'umi';
interface Params {
  page: number;
  pageSize: number;
}
// 获取数据
export const getserch = (params: Params) => {
  return request('/api/search', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};

//删除搜索记录
export const delserch = (id: string) => {
  return request(`/api/search/${id}`, {
    method: 'DELETE',
    skipErrorHandler: true,
  });
};
