import { request } from 'umi';
interface Params {
  page: number;
  pageSize: number;
}
//获取访问数据
export const getview = (params: Params) => {
  return request(`/api/view`, {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
//删除访问数据
export const delview = (id: string) => {
  return request(`/api/view/${id}`, {
    method: 'DELETE',
  });
};
//添加访问数据
export const gaddview = () => {
  return request(`/api/view`, {
    method: 'post',
  });
};
