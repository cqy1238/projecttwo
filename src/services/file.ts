import { request } from 'umi';
interface Params {
  page?: number;
  pageSize?: number;
  originalname?: string;
  type?: string;
}
//获取文件列表
export const getFileList = (params?: Params) => {
  return request('/api/file', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
// 删除文件
export const delFile = (id?: string) => {
  return request(`/api/file/${id}`, {
    method: 'delete',
    skipErrorHandler: true,
  });
};
