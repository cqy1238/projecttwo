import { request } from 'umi';

interface Params {
  page: number;
  pageSize: number;
  name: string;
  email: string;
  adrole: string;
  locstatus: string;
}
// /api/auth/login

export const Rbac = (data?: Params) => {
  return request('/api/article', {
    method: 'get',
    data,
    skipErrorHandler: true,
  });
};
// https://creationapi.shbwyz.com/api/user?page=1&pageSize=12&name=1&email=1&role=admin&status=locked
// &name=${names}&email=${''}&role=${''}&status=${''}
// email=1&role=admin&status=locked
//用户分页接口  搜索接口
export const UserManageList = (params: Params) => {
  return request(`/api/user`, {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
};
//获取文章
export const getAllArticle = (params?: Params) => {
  return request('/api/article', {
    method: 'get',
    params,
  });
};
//禁止启用
export const disenable = (item: any) => {
  return request('/api/user/update', {
    method: 'POST',
    data: item,
    skipErrorHandler: true,
  });
};

//设置
export const Setlist = (item: any) =>
  request('/api/setting', {
    method: 'post',
    data: item,
    skipErrorHandler: true,
  });
//文件
export const fileLists = ({ page, pageSize }: Params) =>
  request(`/api/file?page=${page}&pageSize=${pageSize}`, {
    method: 'get',
    skipErrorHandler: true,
  });

//设置测试接口
// https://creationapi.shbwyz.com/api/smtp
export const Settest = (item: any) =>
  request('/api/smtp', {
    method: 'post',
    data: item,
    skipErrorHandler: true,
  });
