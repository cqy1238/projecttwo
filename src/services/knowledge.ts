import { request } from 'umi';
interface Params {
  page?: number;
  pageSize?: number;
  id?: string;
  status?: string;
}

export const getKnowledgeList = (data?: Params) => {
  return request('/api/knowledge', {
    method: 'get',
    data,
    skipErrorHandler: true,
  });
};
export const getCloudList = (params: Params) => {
  return request(`/api/knowledge/${params.id}`, {
    method: 'patch',
    data: { status: params.status },
    skipErrorHandler: true,
  });
};
export const getDelList = (params: Params) => {
  return request(`/api/knowledge/${params.id}`, {
    method: 'delete',
    data: params,
    skipErrorHandler: true,
  });
};
