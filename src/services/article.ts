import { request } from 'umi';
interface Params {
  [propsName: string]: any;
}
//文章操作
//获取                 api/article        get     {page: 1,pageSize: 12}
//首焦推荐/撤销推荐     api/article:id        PATCH   {isRecommended: true/false}
//删除                 api/article:id     delete
//发布草稿             api/article:id     PATCH    {status: "publish"/"draft"}
export const articleManager = (params: Params) => {
  return request(`/api/article/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};
//分类操作
//获取  api/category       get    无参数
//添加  api/category       post   {"label": "222","value": "222"}
//删除  api/category:id    delete
//更新  pai/category:id    patch  {"label": "222","value": "222"}
export const categoryManager = (params: Params) => {
  return request(`/api/category/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};
//标签操作
//获取  api/tag       get    无参数
//添加  api/tag       post   {"label": "222","value": "222"}
//删除  api/tag:id    delete
//更新  pai/tag:id    patch  {"label": "222","value": "222"}
export const tagsManager = (params: Params) => {
  return request(`/api/tag/${params.id ? params.id : ''}`, {
    method: params.method,
    data: params.data,
    params: params.params,
  });
};

//获取文章
export const getAllArticle = (params: Params) => {
  return request('/api/article', {
    method: 'get',
    params,
  });
};

// 获取分类数据
export const categoryNum = (params: Params) => {
  return request('/api/category', {
    method: 'get',
    params,
  });
};
// 获取标签数据
export const tagNum = (params: Params) => {
  return request('/api/tag', {
    method: 'get',
    params,
  });
};
