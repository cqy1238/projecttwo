import { request } from 'umi';

interface Params {
  page?: number;
  pageSize?: number;
  from?: string;
  to?: string;
  subject?: string;
}

export const getEmail = (params?: Params) => {
  return request('/api/smtp', {
    method: 'GET',
    params,
    skipErrorHandler: true,
  });
};
