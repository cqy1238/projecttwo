import { request } from 'umi';
interface Params {
  // unique: number;
}

export const getOSS = (data: Params) => {
  return request('/api/file/upload', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
};
