import React, { useState, useEffect } from 'react';
import { Table, Space, Button, Spin, message, Form, Input, Select } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import { UserManageList, disenable } from '@/services/usermanager';
// import { request } from 'umi'; //测试阶段
// import axios from 'axios'
import { useRequest } from 'umi';
import styles from './index.less';
import type { PaginationProps } from 'antd';
import { RedoOutlined } from '@ant-design/icons';
import moment from 'moment';
// import useEffect from 'react';
// import me from 'react';
interface Iprops {
  data: any;
}

interface DataType {
  id: string;
  title: string;
  cover: string | null;
  summary: string | null;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount: null | string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: any[];
  category: any;
}
const { Option } = Select;

const App: React.FC<Iprops> = () => {
  const columns: ColumnsType<DataType> = [
    {
      title: '账户',
      dataIndex: 'title',
      fixed: 'left',
      width: '150px',
      // align: 'center',
      render: (text, record: any) => {
        // console.log(record);

        return <>{record.name}</>;
      },
    },
    {
      title: '邮箱',
      dataIndex: 'title',
      width: '200px',
      // fixed: 'left',
      align: 'center',
      render: (text, record: any) => {
        // console.log(record);

        return <>{record.email}</>;
      },
    },
    {
      title: '角色',
      dataIndex: 'status',
      // fixed: 'left',
      align: 'center',
      width: '100px',
      render: (text, record) => {
        return <>{record.role === 'admin' ? '管理员' : '访客'}</>;
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      // fixed: 'left',
      width: '100px',
      // align: 'center',
      render: (text, record) => {
        // console.log(record);

        return (
          <>
            {record.status === 'active' ? (
              <div className={styles.color}>
                <span className={styles.green}></span>
                <span>通过</span>
              </div>
            ) : (
              <div className={styles.color}>
                <span className={styles.orange}></span>
                <span>未通过</span>
              </div>
            )}
          </>
        );
      },
    },
    {
      title: '注册日期',
      dataIndex: 'classify',
      // fixed: 'left',
      // align: 'center',
      render: (text, record) => {
        // console.log(record);

        return <>{moment(record.createAt).format('YYYY-MM-DD HH:mm')}</>;
      },
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '150px',
      render: (text, record) => (
        <Space size="middle">
          {record.status === 'active' ? (
            <a
              onClick={() => {
                disab(record);
              }}
            >
              禁用
            </a>
          ) : (
            <a onClick={() => enable(record)}>启用</a>
          )}
          {record.role === 'admin' ? (
            <a
              onClick={() => {
                noAuthor(record);
              }}
            >
              解决授权
            </a>
          ) : (
            <a onClick={() => Author(record)}>授权</a>
          )}
        </Space>
      ),
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [arr, setArr] = useState([]);
  const [ars, setArs] = useState([]);
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const [page, setPaze] = useState(1); //当前页
  const [pageSize, setPageSize] = useState(8);

  //调用数据
  useEffect(() => {
    UserManageList({ page, pageSize }).then((res) => {
      setArr(res.data);
    });
    return () => {
      UserManageList({ page, pageSize }).then((res) => {
        setArr(res.data);
      });
    };
  }, []);

  //禁用
  const disab = (item: any) => {
    item.status = 'locked';
    // console.log(item);
    disenable(item).then((res) => {
      UserManageList({ page, pageSize }).then((res) => {
        setArr(res.data);
      });
      message.success('操作成功');
    });
  };

  //启用
  const enable = (item: any) => {
    // console.log(22);
    // console.log(item);
    item.status = 'active';
    disenable(item).then((res) => {
      UserManageList({ page, pageSize }).then((res) => {
        setArr(res.data);
      });
    });
    message.success('操作成功');
  };
  //解决授权
  const noAuthor = (item: any) => {
    //  console.log(11);
    item.role = 'visitor';
    disenable(item).then((res) => {
      // console.log(res);
      UserManageList({ page, pageSize }).then((res) => {
        setArr(res.data);
      });
    });
    message.success('操作成功');
  };
  //授权
  const Author = (item: any) => {
    //  console.log(22);
    item.role = 'admin';
    disenable(item).then((res) => {
      // console.log(res);
      UserManageList({ page, pageSize }).then((res) => {
        setArr(res.data);
      });
    });
    message.success('操作成功');
  };
  //全部启用
  const startup = () => {
    setArs(arr[0]);
    ars &&
      ars.forEach((v: any, i: any) => {
        //  console.log(v);
        v.status = 'active';
        disenable(v).then((res) => {
          UserManageList({ page, pageSize }).then((res) => {
            setArr(res.data);
          });
        });
      });
    message.success('操作成功');
  };
  //全部禁用
  const disable = () => {
    setArs(arr[0]);
    ars &&
      ars.forEach((v: any, i: any) => {
        // console.log(v);
        v.status = 'locked';
        disenable(v).then((res) => {
          UserManageList({ page, pageSize }).then((res) => {
            setArr(res.data);
          });
        });
      });
    message.success('操作成功');
  };
  //全部解决授权
  const noAuthors = () => {
    setArs(arr[0]);
    ars &&
      ars.forEach((v: any, i: any) => {
        // console.log(v);
        v.role = 'visitor';
        disenable(v).then((res) => {
          UserManageList({ page, pageSize }).then((res) => {
            setArr(res.data);
          });
        });
      });
    message.success('操作成功');
  };
  //全部授权
  const Authors = () => {
    setArs(arr[0]);
    ars &&
      ars.forEach((v: any, i: any) => {
        // console.log(v);
        v.role = 'admin';
        disenable(v).then((res) => {
          UserManageList({ page, pageSize }).then((res) => {
            setArr(res.data);
          });
        });
      });
    message.success('操作成功');
  };
  //刷新数据
  const lodinglist = () => {
    setLoading(true);
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
    UserManageList({ page, pageSize }).then((res) => {
      setArr(res.data);
    });
  };
  //控制表单
  const rowSelection: TableRowSelection<DataType> = {
    // selectedRowKeys,
    onChange: onSelectChange,
  };
  //页码
  const onShowSizeChange: PaginationProps['onShowSizeChange'] = (
    current,
    pageSize,
  ) => {
    UserManageList({ page, pageSize }).then((res) => {
      // console.log(res);
      setArr(res.data);
    });
    setPaze(current);
    setPageSize(pageSize);
  };
  //搜索
  const onFinish = (values: any) => {
    // console.log('Success:', values);
    UserManageList({
      page,
      pageSize,
      name: values.name,
      email: values.email,
      role: values.role,
      status: values.status,
    }).then((res) => {
      console.log(res.data);
      setArr(res.data);
    });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  //重置
  const onReset = () => {
    form.resetFields();
  };

  const hasSelected = selectedRowKeys.length > 0;

  return (
    <div>
      <div className={styles.userform}>
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="inline"
        >
          <Form.Item label="账户" name="name">
            <Input placeholder="请输入用户账户" />
          </Form.Item>
          <Form.Item label="邮箱" name="email">
            <Input placeholder="请输入账户邮箱" />
          </Form.Item>
          <Form.Item label="角色" name="role" style={{ width: 140 }}>
            <Select allowClear>
              <Option value="admin">管理员</Option>
              <Option value="visitor">访客</Option>
            </Select>
          </Form.Item>
          <Form.Item label="状态" name="status" style={{ width: 140 }}>
            <Select allowClear>
              <Option value="locked">锁定</Option>
              <Option value="active">可用</Option>
            </Select>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button htmlType="button" onClick={onReset}>
              重置
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className={styles.usertablesss}>
        <div style={{ marginBottom: 16 }}>
          {hasSelected ? (
            <div className={styles.actionTop}>
              {selectedRowKeys.length > 0 ? (
                <div className={styles.actionLeft}>
                  <Button style={{ marginRight: '8px' }} onClick={startup}>
                    启用
                  </Button>
                  <Button style={{ marginRight: '8px' }} onClick={disable}>
                    禁用
                  </Button>
                  <Button style={{ marginRight: '8px' }} onClick={noAuthors}>
                    解除授权
                  </Button>
                  <Button style={{ marginRight: '8px' }} onClick={Authors}>
                    授权
                  </Button>
                </div>
              ) : (
                <div></div>
              )}

              <div className={styles.actionRight}>
                <RedoOutlined
                  style={{ fontSize: 18 }}
                  rotate={270}
                  onClick={lodinglist}
                />
              </div>
            </div>
          ) : (
            ''
          )}
        </div>
        <div>
          {loading ? (
            <Spin />
          ) : (
            <Table
              rowKey={(record) => record.id}
              scroll={{ x: 1500 }}
              rowSelection={rowSelection}
              columns={columns}
              dataSource={arr && arr[0]}
              pagination={{
                onChange: onShowSizeChange,
                total: arr && arr[1],
                showTotal: (total) => `共 ${total} 条`,
                defaultPageSize: pageSize, //显示条数
                defaultCurrent: page, //当前页
                pageSizeOptions: [8, 12, 24, 36], //每页的数量
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default App;
