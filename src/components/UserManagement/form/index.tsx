import React, { useState } from 'react';
import { Form, Input, Button, Select } from 'antd';
import styles from './index.less';
import { UserManageList, SEARCH } from '@/services/usermanager';
const { Option } = Select;
const App: React.FC = () => {
  const [form] = Form.useForm();

  //搜索
  const onFinish = (values: any) => {
    // console.log('Success:', values);
    if (values.name) {
      console.log(1);
    }
    if (values.email) {
      console.log(2);
    }
    if (values.role) {
      console.log(3);
    }
    if (values.status) {
      console.log(4);
    }
    // SEARCH(values).then((res) => {
    //   console.log(res.data);
    // });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  //重置
  const onReset = () => {
    form.resetFields();
  };
  return (
    <div className={styles.userform}>
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
      >
        <Form.Item label="账户" name="name">
          <Input placeholder="请输入用户账户" />
        </Form.Item>
        <Form.Item label="邮箱" name="email">
          <Input placeholder="请输入账户邮箱" />
        </Form.Item>
        <Form.Item label="角色" name="role" style={{ width: 140 }}>
          <Select allowClear>
            <Option value="admin">管理员</Option>
            <Option value="visitor">访客</Option>
          </Select>
        </Form.Item>
        <Form.Item label="状态" name="status" style={{ width: 140 }}>
          <Select allowClear>
            <Option value="locked">锁定</Option>
            <Option value="active">可用</Option>
          </Select>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button htmlType="button" onClick={onReset}>
            重置
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default App;
