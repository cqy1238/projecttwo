import React from 'react';
import { Form, Input, Button, Select, Row, Col } from 'antd';
import './index.less';
const { Option } = Select;
const App: React.FC = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="inline"
    >
      <Row style={{ width: '100%', alignContent: 'center' }}>
        <Col span={7}>
          <Form.Item label="称呼" name="name" style={{ width: '100%' }}>
            <Input placeholder="请输入称呼" />
          </Form.Item>
        </Col>
        <Col span={7}>
          <Form.Item label="Email" name="email" style={{ width: '100%' }}>
            <Input placeholder="请输入联系方式" />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item label="状态" name="status">
            <Select placeholder="" allowClear>
              <Option value="true">已通过</Option>
              <Option value="false">未通过</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
        <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button htmlType="button">重置</Button>
        </Form.Item>
      </Row>
    </Form>
  );
};

export default App;
