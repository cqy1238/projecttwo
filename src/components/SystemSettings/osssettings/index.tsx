import React, { useState, useEffect, useRef } from 'react';
// useState, useEffect, useRef
import Editor from '@monaco-editor/react';
import { Alert, Button } from 'antd';
import styles from './index.less';
import markdown from './markdown.less';
import MarkdownIt from 'markdown-it';
// import props from '../../../.umi/plugin-layout/layout/utils/getLayoutContent';

// // 写法1
// const { editor } = ref.current;
// editor.setValue('这里写要待格式化的数据');
// editor.trigger('anyString', 'editor.action.formatDocument');//自动格式化代码
// editor.setValue(editor.getValue());//再次设置

type Props = {};

const arr = {
  adminSystemUrl: 'creationadmin.shbwyz.com',
  baiduAnalyticsId: '8bd099eec64421b1831043373289e0cb',
  createAt: '2021-07-06T01:38:53.946Z',
  googleAnalyticsId: null,
  i18n: '{"en":{"zh":"Chinese","en":"English","serverNotAvaliable":"The server is going to launch a rocket for Musk now 🚀","pageMissing":"This page went to space with Bezos","archives":"Archives","total":"Total","totalSearch":"Totally searched ","piece":" Count","passwd":"Password","wrongPasswd":"Wrong Password","protectedArticleMsg":"The article is protected, please enter the access password","backHome":"Back to Home","confirm":"Confirm","unknownTitle":"Unknown Title","articleCover":"Article Cover","publishAt":"Publish At","readings":"Reading","copyrightInfo":"Copyright Information","copyrightContent":"Non-commercial-Attribution-Freely Reprinted","categoryArticle":"Category","gettingArticle":"Fetching articles...","comment":"Comment","gettingKnowledge":"Fetching knowledge books...","knowledgeBooks":"Knowledge Books","readingCount":"reading","startReading":"Start Reading","pleaseWait":"Coming soon","otherKnowledges":"Other Knowledge Books","unknownKnowledgeChapter":"Unknown Chapter","recommendToReading":"Recommended Readings","yu":"About","tagRelativeArticles":"tag related articles","all":"All","readingCountTemplate":"reading","articleCountTemplate":"count","share":"Share","empty":"No data","categoryTitle":"Category","commentNamespace":{"reply":"Reply","emoji":"Emoji","replyPlaceholder":"Please enter the content of the comment (Markdown is supported)","publish":"Send","close":"Close","commentSuccess":"The comment is successful and has been submitted for review","userInfoTitle":"Please set your information","userInfoName":"Username","userInfoNameValidMsg":"Please tell me your name","userInfoEmail":"Email","userInfoEmailValidMsg":"Please enter correct email","userInfoConfirm":"Save","userInfoCancel":"Cancel"},"loading":"Loading","copySuccess":"Copy successfully","copy":"copy","article":"Articles","searchArticle":"Search Articles","searchArticlePlaceholder":"Enter keywords, search articles","shareNamespace":{"title":"Share Poster","createingPoster":"The poster is being generated, please be patient...","createdPosterSuccess":"The poster is completed.","createdPosterError":"Fail to generate poster.","qrcode":"Scan the QR code to read the article","shareFrom":" Original shared from "},"tagTitle":"Tags","toc":"Toc"},"zh":{"zh":"汉语","en":"英文","serverNotAvaliable":"服务器暂时去给马斯克发射火箭去了🚀","pageMissing":"页面和贝佐斯去太空旅行了~~","archives":"归档","total":"共计","totalSearch":"共搜索到","piece":"篇","passwd":"密码","wrongPasswd":"密码错误","protectedArticleMsg":"文章受保护，请输入访问密码","backHome":"回首页","confirm":"确认","unknownTitle":"未知标题","articleCover":"文章封面","publishAt":"发布于","readings":"阅读量","copyrightInfo":"版权信息","copyrightContent":"非商用-署名-自由转载","categoryArticle":"分类文章","gettingArticle":"正在获取文章...","comment":"评论","gettingKnowledge":"正在获取知识...","knowledgeBooks":"知识小册","readingCount":"次阅读","startReading":"开始阅读","pleaseWait":"敬请期待","otherKnowledges":"其他知识笔记","unknownKnowledgeChapter":"未知章节内容","recommendToReading":"推荐阅读","yu":"与","tagRelativeArticles":"标签有关的文章","all":"所有","readingCountTemplate":"次阅读","articleCountTemplate":"篇文章","share":"分享","empty":"暂无数据","categoryTitle":"文章分类","commentNamespace":{"reply":"回复","emoji":"表情","replyPlaceholder":"请输入评论内容（支持 Markdown）","publish":"发布","close":"收起","commentSuccess":"评论成功，已提交审核","userInfoTitle":"请设置你的信息","userInfoName":"名称","userInfoNameValidMsg":"请输入您的称呼","userInfoEmail":"邮箱","userInfoEmailValidMsg":"输入合法邮箱地址，以便在收到回复时邮件通知","userInfoConfirm":"设置","userInfoCancel":"取消"},"loading":"加载中","copySuccess":"复制成功","copy":"复制","article":"文章","searchArticle":"文章搜索","searchArticlePlaceholder":"输入关键字，搜索文章","shareNamespace":{"title":"分享海报","createingPoster":"海报生成中，请耐心等待...","createdPosterSuccess":"分享海报制作完成。","createdPosterError":"分享海报制作失败，请手动截图","qrcode":"识别二维码查看文章","shareFrom":" 原文分享自"},"tagTitle":"文章标签","toc":"目录"}}',
  id: 'c00bacf1-558c-48c7-9695-05e1ea84be15',
  oss: '{"type":"aliyun","accessKeyId":"LTAI5t9dizVh4xdWARbRhQhS","accessKeySecret":"4vYYKbOK5ibzKe7aVFNe6SuHWi6tWm","bucket":"jinyd","https":false,"region":"oss-cn-beijing"}',
  seoDesc: 'html关键字和app关键字',
  seoKeyword: 'html,app',
  smtpFromUser: 'bwbjwz@163.com',
  smtpHost: 'smtp.163.co1',
  smtpPass: 'MIQGCLSXRSZQNNPO',
  smtpPort: '461',
  smtpUser: 'bwbjwz@163.com',
  systemFavicon:
    'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
  systemFooterInfo:
    '{"en":{"zh":"Chinese","en":"English","serverNotAvaliable":"The server is going to launch a rocket for Musk now 🚀","pageMissing":"This page went to space with Bezos","archives":"Archives","total":"Total","totalSearch":"Totally searched ","piece":" Count","passwd":"Password","wrongPasswd":"Wrong Password","protectedArticleMsg":"The article is protected, please enter the access password","backHome":"Back to Home","confirm":"Confirm","unknownTitle":"Unknown Title","articleCover":"Article Cover","publishAt":"Publish At","readings":"Reading","copyrightInfo":"Copyright Information","copyrightContent":"Non-commercial-Attribution-Freely Reprinted","categoryArticle":"Category","gettingArticle":"Fetching articles...","comment":"Comment","gettingKnowledge":"Fetching knowledge books...","knowledgeBooks":"Knowledge Books","readingCount":"reading","startReading":"Start Reading","pleaseWait":"Coming soon","otherKnowledges":"Other Knowledge Books","unknownKnowledgeChapter":"Unknown Chapter","recommendToReading":"Recommended Readings","yu":"About","tagRelativeArticles":"tag related articles","all":"All","readingCountTemplate":"reading","articleCountTemplate":"count","share":"Share","empty":"No data","categoryTitle":"Category","commentNamespace":{"reply":"Reply","emoji":"Emoji","replyPlaceholder":"Please enter the content of the comment (Markdown is supported)","publish":"Send","close":"Close","commentSuccess":"The comment is successful and has been submitted for review","userInfoTitle":"Please set your information","userInfoName":"Username","userInfoNameValidMsg":"Please tell me your name","userInfoEmail":"Email","userInfoEmailValidMsg":"Please enter correct email","userInfoConfirm":"Save","userInfoCancel":"Cancel"},"loading":"Loading","copySuccess":"Copy successfully","copy":"copy","article":"Articles","searchArticle":"Search Articles","searchArticlePlaceholder":"Enter keywords, search articles","shareNamespace":{"title":"Share Poster","createingPoster":"The poster is being generated, please be patient...","createdPosterSuccess":"The poster is completed.","createdPosterError":"Fail to generate poster.","qrcode":"Scan the QR code to read the article","shareFrom":" Original shared from "},"tagTitle":"Tags","toc":"Toc"},"zh":{"zh":"汉语","en":"英文","serverNotAvaliable":"服务器暂时去给马斯克发射火箭去了🚀","pageMissing":"页面和贝佐斯去太空旅行了~~","archives":"归档","total":"共计","totalSearch":"共搜索到","piece":"篇","passwd":"密码","wrongPasswd":"密码错误","protectedArticleMsg":"文章受保护，请输入访问密码","backHome":"回首页","confirm":"确认","unknownTitle":"未知标题","articleCover":"文章封面","publishAt":"发布于","readings":"阅读量","copyrightInfo":"版权信息","copyrightContent":"非商用-署名-自由转载","categoryArticle":"分类文章","gettingArticle":"正在获取文章...","comment":"评论","gettingKnowledge":"正在获取知识...","knowledgeBooks":"知识小册","readingCount":"次阅读","startReading":"开始阅读","pleaseWait":"敬请期待","otherKnowledges":"其他知识笔记","unknownKnowledgeChapter":"未知章节内容","recommendToReading":"推荐阅读","yu":"与","tagRelativeArticles":"标签有关的文章","all":"所有","readingCountTemplate":"次阅读","articleCountTemplate":"篇文章","share":"分享","empty":"暂无数据","categoryTitle":"文章分类","commentNamespace":{"reply":"回复","emoji":"表情","replyPlaceholder":"请输入评论内容（支持 Markdown）","publish":"发布","close":"收起","commentSuccess":"评论成功，已提交审核","userInfoTitle":"请设置你的信息","userInfoName":"名称","userInfoNameValidMsg":"请输入您的称呼","userInfoEmail":"邮箱","userInfoEmailValidMsg":"输入合法邮箱地址，以便在收到回复时邮件通知","userInfoConfirm":"设置","userInfoCancel":"取消"},"loading":"加载中","copySuccess":"复制成功","copy":"复制","article":"文章","searchArticle":"文章搜索","searchArticlePlaceholder":"输入关键字，搜索文章","shareNamespace":{"title":"分享海报","createingPoster":"海报生成中，请耐心等待...","createdPosterSuccess":"分享海报制作完成。","createdPosterError":"分享海报制作失败，请手动截图","qrcode":"识别二维码查看文章","shareFrom":" 原文分享自"},"tagTitle":"文章标签","toc":"目录"}}',
  systemLogo: 'https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-04-24/4.png',
  systemTitle: '创作平台',
  systemUrl: 'creation.shbwyz.com',
  updateAt: '2022-05-30T03:00:38.000Z',
};

export default function index(props: any) {
  console.log(props.data.oss);

  const [html, setHtml] = useState('');

  const content = useRef<any>(null);
  const handleChange = (val: any) => {
    console.log(val);
    // console.log(content.current.render(val)); // html: <p></p>  text:1231
    // setHtml(content.current.render(val));
  };
  useEffect(() => {
    content.current = new MarkdownIt();
  }, []);

  return (
    <div className={styles.ossseting}>
      <Alert
        message={
          <div>
            <h3>说明</h3>
            <h5>
              请在编辑器中输入您的 oss 配置，并添加 type 字段区分
              "type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""
            </h5>
          </div>
        }
        type="info"
        showIcon
      />
      <div className={styles.index}>
        <Editor
          className={styles.edit}
          height="900"
          language="markdown"
          onChange={handleChange}
          value={props.data && props.data.oss}
        />
        <Button type="primary" onClick={handleChange}>
          提交
        </Button>
      </div>
    </div>
  );
}
