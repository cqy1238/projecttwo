import React, { useState, useEffect } from 'react';
import './index.less';
import { Input, Drawer, Button } from 'antd';
import type { PaginationProps } from 'antd';
import type { DrawerProps } from 'antd/es/drawer';
import { createFromIconfontCN, SnippetsOutlined } from '@ant-design/icons';
import { UploadOutlined } from '@ant-design/icons';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Space, Image, Card, Pagination, Form, Upload } from 'antd';
import type { UploadFile } from 'antd/es/upload/interface';
import { fileLists, Setlist } from '@/services/usermanager';
import { useRequest } from 'umi';
// import useState from 'react';
type Props = {};

const { TextArea } = Input;
const { Meta } = Card;

const fileList: UploadFile[] = [
  {
    uid: '-1',
    name: 'xxx.png',
    status: 'done',
    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    thumbUrl:
      'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  },
];

const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});
// const arrr = {
//   adminSystemUrl: 'creationadmin.shbwyz.com',
//   baiduAnalyticsId: '8bd099eec64421b1831043373289e0cb',
//   createAt: '2021-07-06T01:38:53.946Z',
//   googleAnalyticsId: null,
//   i18n: '{"en":{"zh":"Chinese","en":"English","serverNotAvaliable":"The server is going to launch a rocket for Musk now 🚀","pageMissing":"This page went to space with Bezos","archives":"Archives","total":"Total","totalSearch":"Totally searched ","piece":" Count","passwd":"Password","wrongPasswd":"Wrong Password","protectedArticleMsg":"The article is protected, please enter the access password","backHome":"Back to Home","confirm":"Confirm","unknownTitle":"Unknown Title","articleCover":"Article Cover","publishAt":"Publish At","readings":"Reading","copyrightInfo":"Copyright Information","copyrightContent":"Non-commercial-Attribution-Freely Reprinted","categoryArticle":"Category","gettingArticle":"Fetching articles...","comment":"Comment","gettingKnowledge":"Fetching knowledge books...","knowledgeBooks":"Knowledge Books","readingCount":"reading","startReading":"Start Reading","pleaseWait":"Coming soon","otherKnowledges":"Other Knowledge Books","unknownKnowledgeChapter":"Unknown Chapter","recommendToReading":"Recommended Readings","yu":"About","tagRelativeArticles":"tag related articles","all":"All","readingCountTemplate":"reading","articleCountTemplate":"count","share":"Share","empty":"No data","categoryTitle":"Category","commentNamespace":{"reply":"Reply","emoji":"Emoji","replyPlaceholder":"Please enter the content of the comment (Markdown is supported)","publish":"Send","close":"Close","commentSuccess":"The comment is successful and has been submitted for review","userInfoTitle":"Please set your information","userInfoName":"Username","userInfoNameValidMsg":"Please tell me your name","userInfoEmail":"Email","userInfoEmailValidMsg":"Please enter correct email","userInfoConfirm":"Save","userInfoCancel":"Cancel"},"loading":"Loading","copySuccess":"Copy successfully","copy":"copy","article":"Articles","searchArticle":"Search Articles","searchArticlePlaceholder":"Enter keywords, search articles","shareNamespace":{"title":"Share Poster","createingPoster":"The poster is being generated, please be patient...","createdPosterSuccess":"The poster is completed.","createdPosterError":"Fail to generate poster.","qrcode":"Scan the QR code to read the article","shareFrom":" Original shared from "},"tagTitle":"Tags","toc":"Toc"},"zh":{"zh":"汉语","en":"英文","serverNotAvaliable":"服务器暂时去给马斯克发射火箭去了🚀","pageMissing":"页面和贝佐斯去太空旅行了~~","archives":"归档","total":"共计","totalSearch":"共搜索到","piece":"篇","passwd":"密码","wrongPasswd":"密码错误","protectedArticleMsg":"文章受保护，请输入访问密码","backHome":"回首页","confirm":"确认","unknownTitle":"未知标题","articleCover":"文章封面","publishAt":"发布于","readings":"阅读量","copyrightInfo":"版权信息","copyrightContent":"非商用-署名-自由转载","categoryArticle":"分类文章","gettingArticle":"正在获取文章...","comment":"评论","gettingKnowledge":"正在获取知识...","knowledgeBooks":"知识小册","readingCount":"次阅读","startReading":"开始阅读","pleaseWait":"敬请期待","otherKnowledges":"其他知识笔记","unknownKnowledgeChapter":"未知章节内容","recommendToReading":"推荐阅读","yu":"与","tagRelativeArticles":"标签有关的文章","all":"所有","readingCountTemplate":"次阅读","articleCountTemplate":"篇文章","share":"分享","empty":"暂无数据","categoryTitle":"文章分类","commentNamespace":{"reply":"回复","emoji":"表情","replyPlaceholder":"请输入评论内容（支持 Markdown）","publish":"发布","close":"收起","commentSuccess":"评论成功，已提交审核","userInfoTitle":"请设置你的信息","userInfoName":"名称","userInfoNameValidMsg":"请输入您的称呼","userInfoEmail":"邮箱","userInfoEmailValidMsg":"输入合法邮箱地址，以便在收到回复时邮件通知","userInfoConfirm":"设置","userInfoCancel":"取消"},"loading":"加载中","copySuccess":"复制成功","copy":"复制","article":"文章","searchArticle":"文章搜索","searchArticlePlaceholder":"输入关键字，搜索文章","shareNamespace":{"title":"分享海报","createingPoster":"海报生成中，请耐心等待...","createdPosterSuccess":"分享海报制作完成。","createdPosterError":"分享海报制作失败，请手动截图","qrcode":"识别二维码查看文章","shareFrom":" 原文分享自"},"tagTitle":"文章标签","toc":"目录"}}',
//   id: 'c00bacf1-558c-48c7-9695-05e1ea84be15',
//   oss: '{"type":"aliyun","accessKeyId":"LTAI5t9dizVh4xdWARbRhQhS","accessKeySecret":"4vYYKbOK5ibzKe7aVFNe6SuHWi6tWm","bucket":"jinyd","https":false,"region":"oss-cn-beijing"}',
//   seoDesc: 'html关键字和app关键字',
//   seoKeyword: 'html,app',
//   smtpFromUser: 'bwbjwz@163.com',
//   smtpHost: 'smtp.163.co1',
//   smtpPass: 'MIQGCLSXRSZQNNPO',
//   smtpPort: '461',
//   smtpUser: 'bwbjwz@163.com',
//   systemFavicon:
//     'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
//   systemFooterInfo:
//     '{"en":{"zh":"Chinese","en":"English","serverNotAvaliable":"The server is going to launch a rocket for Musk now 🚀","pageMissing":"This page went to space with Bezos","archives":"Archives","total":"Total","totalSearch":"Totally searched ","piece":" Count","passwd":"Password","wrongPasswd":"Wrong Password","protectedArticleMsg":"The article is protected, please enter the access password","backHome":"Back to Home","confirm":"Confirm","unknownTitle":"Unknown Title","articleCover":"Article Cover","publishAt":"Publish At","readings":"Reading","copyrightInfo":"Copyright Information","copyrightContent":"Non-commercial-Attribution-Freely Reprinted","categoryArticle":"Category","gettingArticle":"Fetching articles...","comment":"Comment","gettingKnowledge":"Fetching knowledge books...","knowledgeBooks":"Knowledge Books","readingCount":"reading","startReading":"Start Reading","pleaseWait":"Coming soon","otherKnowledges":"Other Knowledge Books","unknownKnowledgeChapter":"Unknown Chapter","recommendToReading":"Recommended Readings","yu":"About","tagRelativeArticles":"tag related articles","all":"All","readingCountTemplate":"reading","articleCountTemplate":"count","share":"Share","empty":"No data","categoryTitle":"Category","commentNamespace":{"reply":"Reply","emoji":"Emoji","replyPlaceholder":"Please enter the content of the comment (Markdown is supported)","publish":"Send","close":"Close","commentSuccess":"The comment is successful and has been submitted for review","userInfoTitle":"Please set your information","userInfoName":"Username","userInfoNameValidMsg":"Please tell me your name","userInfoEmail":"Email","userInfoEmailValidMsg":"Please enter correct email","userInfoConfirm":"Save","userInfoCancel":"Cancel"},"loading":"Loading","copySuccess":"Copy successfully","copy":"copy","article":"Articles","searchArticle":"Search Articles","searchArticlePlaceholder":"Enter keywords, search articles","shareNamespace":{"title":"Share Poster","createingPoster":"The poster is being generated, please be patient...","createdPosterSuccess":"The poster is completed.","createdPosterError":"Fail to generate poster.","qrcode":"Scan the QR code to read the article","shareFrom":" Original shared from "},"tagTitle":"Tags","toc":"Toc"},"zh":{"zh":"汉语","en":"英文","serverNotAvaliable":"服务器暂时去给马斯克发射火箭去了🚀","pageMissing":"页面和贝佐斯去太空旅行了~~","archives":"归档","total":"共计","totalSearch":"共搜索到","piece":"篇","passwd":"密码","wrongPasswd":"密码错误","protectedArticleMsg":"文章受保护，请输入访问密码","backHome":"回首页","confirm":"确认","unknownTitle":"未知标题","articleCover":"文章封面","publishAt":"发布于","readings":"阅读量","copyrightInfo":"版权信息","copyrightContent":"非商用-署名-自由转载","categoryArticle":"分类文章","gettingArticle":"正在获取文章...","comment":"评论","gettingKnowledge":"正在获取知识...","knowledgeBooks":"知识小册","readingCount":"次阅读","startReading":"开始阅读","pleaseWait":"敬请期待","otherKnowledges":"其他知识笔记","unknownKnowledgeChapter":"未知章节内容","recommendToReading":"推荐阅读","yu":"与","tagRelativeArticles":"标签有关的文章","all":"所有","readingCountTemplate":"次阅读","articleCountTemplate":"篇文章","share":"分享","empty":"暂无数据","categoryTitle":"文章分类","commentNamespace":{"reply":"回复","emoji":"表情","replyPlaceholder":"请输入评论内容（支持 Markdown）","publish":"发布","close":"收起","commentSuccess":"评论成功，已提交审核","userInfoTitle":"请设置你的信息","userInfoName":"名称","userInfoNameValidMsg":"请输入您的称呼","userInfoEmail":"邮箱","userInfoEmailValidMsg":"输入合法邮箱地址，以便在收到回复时邮件通知","userInfoConfirm":"设置","userInfoCancel":"取消"},"loading":"加载中","copySuccess":"复制成功","copy":"复制","article":"文章","searchArticle":"文章搜索","searchArticlePlaceholder":"输入关键字，搜索文章","shareNamespace":{"title":"分享海报","createingPoster":"海报生成中，请耐心等待...","createdPosterSuccess":"分享海报制作完成。","createdPosterError":"分享海报制作失败，请手动截图","qrcode":"识别二维码查看文章","shareFrom":" 原文分享自"},"tagTitle":"文章标签","toc":"目录"}}',
//   systemLogo: 'https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-04-24/4.png',
//   systemTitle: '创作平台',
//   systemUrl: 'creation.shbwyz.com',
//   updateAt: '2022-05-30T03:00:38.000Z',
// };
// console.log(arr.adminSystemUrl);

function index(props: any) {
  console.log(props.data, '333333333333333');

  const [visible, setVisible] = useState(false);
  const [size, setSize] = useState<DrawerProps['size']>();
  const [page, setPaze] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(8);
  // const { data } = useRequest(fileLists);
  const [arr, setArr] = useState([]);
  // let [ars, setArs] = useState([]);
  const [total, setTotal] = useState(0);
  const [form] = Form.useForm();
  // const [, forceUpdate] = useState({});

  //暂时用
  const [ste, useSte] = useState<any>([]);
  // To disable submit button at the beginning.
  useEffect(() => {
    Setlist().then((res) => {
      console.log(res.data, '********************************');
      useSte(res.data);
    });
    return () => {
      Setlist().then((res) => {
        // console.log(res.data,'********************************');
        useSte(res.data);
      });
    };
    // console.log(ste);
  }, []);
  // useEffect(() => {
  //   setArs(props.data);
  //   console.log(ars, '获取的数据');
  // });

  const onFinish = (values: any) => {
    console.log('Finish:', values);
  };
  useEffect(() => {
    fileLists({ page, pageSize }).then((res) => {
      setArr(res.data[0]);
      setTotal(res.data);
    });
  }, []);
  const showDefaultDrawer = () => {
    setSize('default');
    setVisible(true);
  };
  const onShowSizeChange: PaginationProps['onShowSizeChange'] = (
    current,
    pageSize,
  ) => {
    console.log(current, pageSize);
    fileLists({ page, pageSize }).then((res) => {
      // console.log(res);
      setArr(res.data[0]);
    });
    setPaze(current);
    setPageSize(pageSize);
  };

  const showLargeDrawer = () => {
    setSize('large');
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  console.log(ste, '接口请求的数据');

  return (
    <div className="system">
      <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item name="systemUrl" label="系统地址">
          <Input
            placeholder="input placeholder"
            defaultValue={ste && ste.baiduAnalyticsId}
          />
        </Form.Item>
        <Form.Item name="adminSystemUrl" label="后台地址">
          <Input
            placeholder="input placeholder"
            defaultValue={props.data && props.data.adminSystemUrl}
          />
        </Form.Item>
        <Form.Item name="systemTitle" label="系统标题">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item name="systemLogo" label="Logo">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item name="systemFavicon" label="Favicon">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item name="systemFooterInfo" label="页脚信息">
          <Input.TextArea />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              保存
            </Button>
          </Space>
        </Form.Item>
      </Form>
      <Drawer
        // title={`${size} Drawer`}
        placement="right"
        size={size}
        onClose={onClose}
        visible={visible}
        extra={
          <Space>
            <h2>文件选择</h2>
            {/* <Button onClick={onClose}>Cancel</Button> */}
            {/* <Button type="primary" onClick={onClose}>
              OK
            </Button> */}
          </Space>
        }
      >
        <div>
          <Form
            form={form}
            name="horizontal_login"
            layout="inline"
            onFinish={onFinish}
          >
            <Form.Item
              label="文件名"
              name="username"
              rules={[
                { required: true, message: 'Please input your username!' },
              ]}
            >
              <Input
                // prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="请输入文件名称"
              />
            </Form.Item>
            <Form.Item
              label="文件类"
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input
                // prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="请输入文件类型"
              />
            </Form.Item>
            <Form.Item shouldUpdate>
              {() => (
                <Button type="primary" htmlType="submit">
                  Log in
                </Button>
              )}
            </Form.Item>
          </Form>
        </div>
        <div id="useupload">
          <Upload
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            listType="picture"
            // defaultFileList={[...fileList]}
          >
            <Button>上传文件</Button>
          </Upload>
        </div>
        <div className="fileimg">
          {arr &&
            arr.map((v: any, i: number) => {
              return (
                <Card
                  key={i}
                  style={{
                    width: '15%',
                    height: '150px',
                    margin: '0 2px -25px 2px',
                  }}
                  hoverable={true}
                  cover={
                    <Image
                      // preview={false}
                      src={v.url}
                    />
                  }
                >
                  <Meta title={v.originalname} />
                </Card>
              );
            })}
        </div>
        <Pagination
          total={total && total[1]}
          showTotal={(total) => `共有${total}条数据`}
          // defaultPageSize={[8,12,24,36]}
          defaultCurrent={1}
          // pageSizeOptions={[8, 12, 24, 36]}
          pageSizeOptions={[8, 10, 12, 24, 36]} //每页的数量
          // onShowSizeChange={onShowSizeChange}
          onChange={onShowSizeChange}
        />
      </Drawer>
    </div>
  );
}

export default index;
