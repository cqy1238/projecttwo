import React, { useRef, useState } from 'react';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

const App: React.FC = (props: any) => {
  console.log('====================================');
  console.log(props.data);
  console.log('====================================');

  const initialPanes = [
    { title: 'en', content: props.data.i18n, key: '1' },
    { title: 'zh', content: 'Content of Tab 2', key: '2' },
  ];

  const [activeKey, setActiveKey] = useState(initialPanes[0].key);
  const [panes, setPanes] = useState(initialPanes);
  const newTabIndex = useRef(0);

  const onChange = (newActiveKey: string) => {
    setActiveKey(newActiveKey);
  };

  const add = () => {
    const newActiveKey = `newTab${newTabIndex.current++}`;
    const newPanes = [...panes];
    newPanes.push({
      title: 'New Tab',
      content: 'Content of new Tab',
      key: newActiveKey,
    });
    setPanes(newPanes);
    setActiveKey(newActiveKey);
  };

  const remove = (targetKey: string) => {
    let newActiveKey = activeKey;
    let lastIndex = -1;
    panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const newPanes = panes.filter((pane) => pane.key !== targetKey);
    if (newPanes.length && newActiveKey === targetKey) {
      if (lastIndex >= 0) {
        newActiveKey = newPanes[lastIndex].key;
      } else {
        newActiveKey = newPanes[0].key;
      }
    }
    setPanes(newPanes);
    setActiveKey(newActiveKey);
  };

  const onEdit = (targetKey: string, action: 'add' | 'remove') => {
    if (action === 'add') {
      add();
    } else {
      remove(targetKey);
    }
  };

  return (
    <Tabs
      type="editable-card"
      onChange={onChange}
      activeKey={activeKey}
      onEdit={onEdit}
    >
      {panes.map((pane) => (
        <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
          {pane.content}
        </TabPane>
      ))}
    </Tabs>
  );
};

export default App;

// import React,{ useState, useEffect, useRef } from 'react';
// // useState, useEffect, useRef
// import Editor from '@monaco-editor/react'
// import { Alert ,Button } from 'antd';
// import styles from './index.less'
// import markdown from './markdown.less';
// import MarkdownIt from 'markdown-it';
// // import props from '../../../.umi/plugin-layout/layout/utils/getLayoutContent';

// // // 写法1
// // const { editor } = ref.current;
// // editor.setValue('这里写要待格式化的数据');
// // editor.trigger('anyString', 'editor.action.formatDocument');//自动格式化代码
// // editor.setValue(editor.getValue());//再次设置

// type Props = {};

// export default function index(props : any) {
//   console.log(props.data.oss);

//   const [html, setHtml] = useState('');

//   const content = useRef<any>(null);
//   const handleChange = (val: any) => {
//     console.log(val);
//     // console.log(content.current.render(val)); // html: <p></p>  text:1231
//     // setHtml(content.current.render(val));
//   };
//   useEffect(() => {
//     content.current = new MarkdownIt();
//   }, []);

//   return (
//     <div className={styles.ossseting} >
//       <Alert
//         message={
//           <div>
//             <h3>说明</h3>
//             <h5>
//               请在编辑器中输入您的 oss 配置，并添加 type 字段区分
//               "type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""
//             </h5>
//           </div>
//         }
//         type="info"
//         showIcon
//       />
//      <div className={styles.index}>
//         <Editor
//         className={styles.edit}
//            height='900'
//            language='markdown'
//            onChange={handleChange}
//            value={props.data&&props.data.oss}
//         />
//       <Button type="primary" onClick={handleChange}>提交</Button>
//      </div>

//     </div>
//   );
// }
