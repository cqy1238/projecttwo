import React, { useState, useEffect } from 'react';
import { Tabs } from 'antd';
import Systems from '@/components/SystemSettings/system/';
import Internation from '@/components/SystemSettings/Internationalization settings/index';
import Seosettings from '@/components/SystemSettings/Seosettings/index';
import Statistice from '@/components/SystemSettings/Statistics/index';
import Osssettings from '@/components/SystemSettings/osssettings/index';
import Smtpservice from '@/components/SystemSettings/Smtpservice/index';
import System from './index.less';
// import { request } from 'umi';
import { Setlist } from '@/services/usermanager';

import { useRequest } from 'umi';

const { TabPane } = Tabs;

const onChange = (key: string) => {
  // console.log(key);
};
const App: React.FC = () => {
  const { data } = useRequest(Setlist);
  // console.log(data,'总数据');
  // useEffect(()=>{
  //     return ()=>{
  //       const { data } = useRequest(Setlist);
  //     }
  // },[])

  return (
    <Tabs
      defaultActiveKey="1"
      onChange={onChange}
      tabPosition="left"
      className={System.main}
    >
      <TabPane tab="系统设置" key="1">
        <Systems data={data && data}></Systems>
      </TabPane>
      <TabPane tab="国际化设置" key="2">
        <Internation data={data && data}></Internation>
      </TabPane>
      <TabPane tab="SEO设置" key="3">
        <Seosettings data={data && data}></Seosettings>
      </TabPane>
      <TabPane tab="数据统计" key="4">
        <Statistice data={data && data}></Statistice>
      </TabPane>
      <TabPane tab="OSS设置" key="5">
        <Osssettings data={data && data}></Osssettings>
      </TabPane>
      <TabPane tab="SMTP设置" key="6">
        <Smtpservice data={data && data}></Smtpservice>
      </TabPane>
    </Tabs>
  );
};

export default App;
