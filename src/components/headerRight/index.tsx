import { useState, useEffect } from 'react';
import { GithubOutlined, UserOutlined } from '@ant-design/icons';
import { Menu, Dropdown, Avatar } from 'antd';
import './index.less';
const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: <a href="/ownspace">个人中心</a>,
      },
      {
        key: '2',
        label: <a href="/UserManager">用户管理</a>,
      },
      {
        key: '3',
        label: <a href="/SystemSettings">系统设置</a>,
      },
      {
        key: '4',
        label: <a href="/login">退出登入</a>,
      },
    ]}
  />
);
const HeaderRight = () => {
  const [localName, setLocalName]: any = useState({});

  // setLocalName(userinfo.name)
  useEffect(() => {
    let userinfo = JSON.parse(localStorage.getItem('userinfo')!);
    setLocalName(userinfo);
  }, []);
  return (
    <div className="rightContext">
      <GithubOutlined style={{ fontSize: 24, color: '#535456' }} />
      <Dropdown overlay={menu} placement="bottom">
        <div className="person">
          <Avatar size="small" icon={<UserOutlined />} />
          <span>Hi,{localName ? localName.name : '请登入'}</span>
        </div>
      </Dropdown>
    </div>
  );
};

export default HeaderRight;
