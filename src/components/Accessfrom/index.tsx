import React from 'react';
import { Form, Input, Button, Select } from 'antd';
import style from './index.less';
interface obj {
  [name: string]: string;
}
interface props {
  serchfrom(from: obj): void;
}
const App = (props: props) => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
    props.serchfrom(values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.accfrom}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
      >
        <div className={style.search}>
          <Form.Item label="IP" name="ip">
            <Input placeholder="请输入IP地址" />
          </Form.Item>
          <Form.Item label="UA" name="ui">
            <Input placeholder="请输入User" />
          </Form.Item>
          <Form.Item label="URL" name="url">
            <Input placeholder="请输入URL" />
          </Form.Item>
          <Form.Item label="地址" name="gps">
            <Input placeholder="请输入地址" />
          </Form.Item>
          <Form.Item label="浏览器" name="coml">
            <Input placeholder="请输入浏览器" />
          </Form.Item>
          <Form.Item label="内核" name="cmol">
            <Input placeholder="请输入内核" />
          </Form.Item>
          <Form.Item label="OS" name="os">
            <Input placeholder="请输入操作系统" />
          </Form.Item>
          <Form.Item label="设备" name="">
            <Input placeholder="请输入设备" />
          </Form.Item>
        </div>

        <div className={style.butom}>
          <div></div>
          <div className={style.butombt}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button
                htmlType="button"
                onClick={() => {
                  props.serchfrom({
                    ip: '',
                    ui: '',
                    gps: '',
                    url: '',
                    coml: '',
                    cmol: '',
                    os: '',
                    del: 'true',
                  });
                }}
              >
                重置
              </Button>
            </Form.Item>
          </div>
        </div>
      </Form>
    </div>
  );
};

export default App;
