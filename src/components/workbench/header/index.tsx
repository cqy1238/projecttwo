import React from 'react';
import style from './index.less';
const Header: React.FC = () => {
  const userInfo =
    localStorage.getItem('userinfo') &&
    JSON.parse(localStorage.getItem('userinfo')!);
  return (
    <header className={style.header}>
      <div>
        <p>工作台</p>
        <h1>您好，{userInfo.name}</h1>
        <p>您的角色：{userInfo.role === 'admin' ? '管理员' : '访客'}</p>
      </div>
    </header>
  );
};

export default Header;
