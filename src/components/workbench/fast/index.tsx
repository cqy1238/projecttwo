import React from 'react';
import style from './index.less';
import { useHistory } from 'umi';
// 跳转路由
// const history = useHistory();
const navArr = [
  { title: '文章管理', id: '12340' },
  { title: '评论管理', id: '12341' },
  { title: '文件管理', id: '12342' },
  { title: '用户管理', id: '12343' },
  { title: '访问管理', id: '12344' },
  { title: '系统设置', id: '12345' },
];
const pathArr = [
  '/ArticleManager/allaticle',
  '/CommentsManager',
  '/FileManager',
  '/UserManager',
  '/AccessStatistics',
  '/SystemSettings',
];
const Fast: React.FC = () => {
  return (
    <div className={style.fast}>
      <div className={style.fastNav}>快速导航</div>
      <div className={style.navItem}>
        {navArr &&
          navArr.map((item, index) => {
            return <a key={item.id}>{item.title}</a>;
          })}
      </div>
    </div>
  );
};

export default Fast;
