import React, { useState, useEffect, useCallback } from 'react';
import style from './index.less';
import { Table, Space, Badge, Popconfirm, message, Popover } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { useHistory, useRequest } from 'umi';
import {
  getCommentList,
  getPatchList,
  getDeleteList,
  getReplyList,
} from '@/services/comment';
// 回复评论弹框
import ReplyModal from './replyModal/index';
interface DataType {
  content?: string;
  createAt?: string;
  email?: string;
  hostId?: string;
  html?: string;
  id: string;
  name?: string;
  parentCommentId?: null;
  pass?: boolean;
  replyUserEmail?: null;
  replyUserName?: null;
  updateAt?: string;
  url?: string;
  userAgent?: string;
}
interface ColumnsTypes {
  title: string;
  dataIndex: string;
  key: string;
  render: any;
  fixed: string;
}

const content = (
  <div className={style.articleView}>
    <div>
      <h1>404</h1>
      <span>This page could not be found.</span>
    </div>
  </div>
);
const LatestComments: React.FC = () => {
  let { role, name } = JSON.parse(localStorage.getItem('userinfo') as string);
  const [commitData, setCommitData]: any[] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const history = useHistory();
  const { run, data } = useRequest(getCommentList, { manual: true });
  const { run: passRun } = useRequest(getPatchList, { manual: true });
  const { run: delRun } = useRequest(getDeleteList, { manual: true });
  const { run: replyRun } = useRequest(getReplyList, { manual: true });
  const [recordList, setrecordList] = useState({
    content: '',
    id: '',
    name: '',
    email: '',
    createAt: '',
    hostId: '',
    parentCommentId: '',
    url: '',
  });
  // 封装数据接口请求
  const getLiat = async () => {
    let res = await run({ page: 1, pageSize: 6 });
    setCommitData(res[0]);
  };
  const init = useCallback(async () => {
    let res = await run({ page: 1, pageSize: 6 });
    setCommitData(res[0]);
  }, []);
  useEffect(() => {
    init();
  }, [init]);
  // 点击通过 / 拒绝
  const handleAdopt = async ({ record, passFlag }: any) => {
    let res = await passRun({ id: record.id, pass: passFlag });
    let userinfo =
      localStorage.getItem('userinfo') &&
      JSON.parse(localStorage.getItem('userinfo')!);
    if (userinfo.role === 'admin') {
      getLiat();
      message.success(passFlag ? '评论已通过' : '评论已拒绝');
    }
  };
  // 点击回复显示弹框
  const handleReply = (record: any) => {
    setIsModalVisible(true);
    setrecordList(record);
  };
  // 回复弹框确认按钮
  const changeContent = (value: string) => {
    replyRun({
      content: value,
      createByAdmin: true,
      email: `${name}@${name}.com`,
      hostId: recordList.hostId,
      name: name,
      parentCommentId: recordList.parentCommentId,
      replyUserEmail: recordList.email,
      replyUserName: name,
      url: recordList.url,
    }).then(() => {
      getLiat();
    });
  };
  // 点击删除确认按钮
  const confirm = (id: string) => {
    delRun({ id: id }).then((res: any) => {
      getLiat();
    });
    message.success('评论删除成功');
  };
  // 点击删除取消按钮
  const cancel = (e: any) => {};
  const columns: any = [
    {
      title: '用户',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left',
      render: (text: string, record: any) => (
        <div>
          <span>{text}</span> 在{' '}
          <Popover
            placement="right"
            title={'页面预览'}
            content={content}
            trigger="hover"
          >
            <a href="">文章</a>
          </Popover>{' '}
          评论{' '}
          <Popover
            content={record.content}
            title="评论详情-原始内容"
            trigger="hover"
          >
            <a>查看内容</a>
            {''}
          </Popover>
          {record.pass === true ? (
            <span style={{ paddingLeft: '15px' }}>
              <Badge status="success" />
              通过
            </span>
          ) : (
            <span style={{ paddingLeft: '15px' }}>
              <Badge status="warning" />
              未通过
            </span>
          )}
        </div>
      ),
    },
    {
      title: '操作',
      dataIndex: 'action',
      key: 'action',
      fixed: 'right',
      render: (_: any, record: any) => (
        <Space size="middle" className={style.table}>
          <a
            onClick={() => {
              handleAdopt({ record, passFlag: true });
            }}
          >
            通过
          </a>
          <a
            onClick={() => {
              handleAdopt({ record, passFlag: false });
            }}
          >
            拒绝
          </a>
          <a
            onClick={() => {
              handleReply(record);
            }}
          >
            回复
          </a>
          <Popconfirm
            title="确认删除这个评论？"
            onConfirm={() => {
              confirm(record.id);
            }}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <div className={style.latestComments}>
      <div className={style.latestCommentsNav}>
        <div>最新评论</div>
        <div
          style={{ fontSize: '14px' }}
          onClick={() => {
            history.push('/CommentsManager');
          }}
        >
          全部评论
        </div>
      </div>
      <div className={style.latestComments_table}>
        <Table
          rowKey={(record: DataType) => record.id}
          columns={columns}
          dataSource={commitData}
          showHeader={false}
          pagination={false}
        />
        ;
      </div>
      <ReplyModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        changeContent={changeContent}
      />
    </div>
  );
};

export default LatestComments;
function async(arg0: (id: string) => void) {
  throw new Error('Function not implemented.');
}
