import React, { useState } from 'react';
import { Modal, Input } from 'antd';

interface Iprops {
  isModalVisible: boolean;
  setIsModalVisible: any;
}
const { TextArea } = Input;
const ReplyModal = (props: any) => {
  const [value, setValue] = useState('');
  const handleOk = () => {
    props.setIsModalVisible(false);
    props.changeContent(value);
  };

  const handleCancel = () => {
    props.setIsModalVisible(false);
  };

  return (
    <>
      <Modal
        title="回复评论"
        visible={props.isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="回复"
      >
        <TextArea
          value={value}
          onChange={(e) => setValue(e.target.value)}
          placeholder="支持 Markdown"
          autoSize={{ minRows: 6, maxRows: 10 }}
        />
      </Modal>
    </>
  );
};
export default ReplyModal;
