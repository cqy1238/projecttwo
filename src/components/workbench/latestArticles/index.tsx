import React, { useState, useEffect, useCallback } from 'react';
import { useRequest, useHistory, useSelector, useDispatch, connect } from 'umi';
import style from './index.less';
import { getAllArticle } from '@/services/article';
import { Card } from 'antd';

const { Meta } = Card;
interface DataItem {
  category: string;
  cover: string;
  createAt: string;
  html: string;
  id: string;
  isCommentable: boolean;
  isPay: boolean;
  isRecommended: boolean;
  likes: number;
  needPassword: boolean;
  publishAt: string;
  status: string;
  summary: null;
  tags: any;
  title: string;
  toc: string;
  totalAmount: null;
  updateAt: string;
  views: number;
}

const LatesArtiles: React.FC = (props: any) => {
  const [articleData, setArticleData] = useState([]);
  const history = useHistory();
  const { loading, run } = useRequest(getAllArticle, {
    manual: true,
  });
  // const dispatch = useDispatch();
  // const state = useSelector((state: any) => state.workbench);
  // console.log(state);

  let init = useCallback(async () => {
    let res = await run({ page: 1, pageSize: 6 });
    setArticleData(res[0]);
  }, []);
  useEffect(() => {
    init();
  }, [init]);
  return (
    <div className={style.latesArtiles}>
      <div className={style.latesArtilesNav}>
        <div>最新文章</div>
        <div
          style={{ fontSize: '14px' }}
          onClick={() => history.push('/ArticleManager/allaticle')}
        >
          全部文章
        </div>
      </div>
      <div className={style.latesArtiles_img}>
        {articleData &&
          articleData.map((item: DataItem, index: number) => {
            return (
              <dl key={item.id} className={style.dl}>
                <dt>
                  <img
                    src={item.cover}
                    alt=""
                    style={{ width: '90%', height: '144px' }}
                  />
                </dt>
                <dd>{item.title}</dd>
              </dl>
            );
            //  (
            //   <Card
            //     key={item.id}
            //     hoverable={true}
            //     bordered={false}
            //     style={{
            //       width: '33.1%',
            //       textAlign: 'center',
            //       borderRight: '1px solid #f0f0f0',
            //       borderBottom: '1px solid #f0f0f0',
            //       marginBottom: '3px',
            //     }}
            //     cover={
            //       <img
            //         alt="example"
            //         src={item.cover}
            //         style={{ height: '144px', padding: '0 22px' }}
            //       />
            //     }
            //   >
            //     <Meta title={item.title} />
            //   </Card>
            // );
          })}
      </div>
    </div>
  );
};

export default LatesArtiles;
