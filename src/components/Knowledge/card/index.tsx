import React, { useState, useEffect } from 'react';
import Drawer from '../Drawer/drawer';
import { Card, Button, Pagination, Tooltip, Popconfirm, message } from 'antd';
import { useRequest } from 'umi';
import {
  getKnowledgeList,
  getCloudList,
  getDelList,
} from '@/services/knowledge';
import {
  EditOutlined,
  SettingOutlined,
  CloudDownloadOutlined,
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import style from './index.less';
const { Meta } = Card;

const Cards = () => {
  // let { role } = JSON.parse(localStorage.getItem('userinfo') as string);
  const { data, run: runlist } = useRequest(() => getKnowledgeList());
  const { run } = useRequest(getCloudList, { manual: true });
  const { run: delrun } = useRequest(getDelList, { manual: true });
  const [visible, setVisible] = useState(false); //控制设置外层弹窗
  const [item, setitem] = useState([]);
  //设为草稿
  const setcloud = (item: any) => {
    if (item.status === 'publish') {
      console.log(item.status);
      run({ id: item.id, status: 'draft' });
    } else if (item.status === 'draft') {
      console.log(item.status);
      run({ id: item.id, status: 'publish' });
    }
    runlist();
  };
  //删除
  const confirm = (item: any) => {
    delrun(item);
    runlist();
  };
  //点击设置
  const openDrawer = (item: any) => {
    setitem(item);
    setVisible(true);
  };
  const closeDrawer = () => {
    setVisible(false);
  };
  return (
    <div>
      {/* 设置弹窗 */}
      <Drawer visible={visible} closeDrawer={closeDrawer} item={item} />
      <div className={style.AddBox}>
        <Button
          key="button"
          icon={<PlusOutlined />}
          type="primary"
          onClick={() => {
            setVisible(true);
          }}
        >
          新建
        </Button>
      </div>
      <div className={style.card}>
        {data &&
          data[0].map((item: any, index: number) => {
            return (
              <Card
                key={index}
                style={{
                  width: 300,
                  height: 280,
                  marginBottom: 20,
                  marginRight: 10,
                }}
                cover={
                  <img alt="example" src={item.cover} className={style.img} />
                }
                actions={[
                  <EditOutlined key="edit" />,
                  <Tooltip
                    placement="top"
                    title={item.status === 'publish' ? '设为草稿' : '发布上线'}
                  >
                    <CloudDownloadOutlined
                      key="cloudDown"
                      onClick={() => setcloud(item)}
                    />
                  </Tooltip>,
                  <SettingOutlined
                    key="setting"
                    onClick={() => openDrawer(item)}
                  />,
                  <Popconfirm
                    title="确定删除?"
                    okText="确定"
                    cancelText="取消"
                    onConfirm={() => confirm(item)}
                  >
                    <DeleteOutlined key="delete" />
                  </Popconfirm>,
                ]}
              >
                <Meta
                  title={item.title}
                  description={item.summary}
                  style={{
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                  }}
                />
              </Card>
            );
          })}
      </div>
      <div className={style.page}>
        <Pagination
          total={data && data[1]}
          showTotal={(total) => `共 ${total} 条`}
          defaultPageSize={12}
          defaultCurrent={1}
        />
      </div>
    </div>
  );
};

export default Cards;
