import React, { useState } from 'react';
import style from './drawer.less';
import './drawer.less';
import { Drawer, Select } from 'antd';
import Forms from '../Froms/index';
import { CloseOutlined } from '@ant-design/icons';

import FileManager from '../../../pages/FileManager/index';
type Props = {
  [propsname: string]: any;
};

export default function Drawerindex(props: Props) {
  const { visible, closeDrawer, item } = props;
  console.log(item);

  const [value, setValue] = useState('');
  const [childrenDrawer, setChildrenDrawer] = useState(false); //控制设置内层弹窗
  //点击关闭弹窗关闭
  const onClose = () => {
    closeDrawer();
  };
  const showChildrenDrawer = (flag: boolean) => {
    setChildrenDrawer(flag);
  };

  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };
  return (
    <div>
      <Drawer
        title="新建知识库"
        width={600}
        closable={false}
        onClose={onClose}
        visible={visible}
        extra={<CloseOutlined onClick={onClose} />}
      >
        <Forms showChildrenDrawer={showChildrenDrawer} item={item}></Forms>
        <Drawer
          title="文件选择"
          width={800}
          closable={false}
          onClose={onChildrenDrawerClose}
          visible={childrenDrawer}
          extra={<CloseOutlined onClick={onChildrenDrawerClose} />}
        >
          <FileManager type="knowledge" />
        </Drawer>
      </Drawer>
    </div>
  );
}
