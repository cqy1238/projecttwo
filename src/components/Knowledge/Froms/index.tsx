import React, { useState } from 'react';
import style from './index.less';
import { Button, Form, Switch, Upload, Input } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
const { TextArea } = Input;
type Props = {
  showChildrenDrawer: (flag: boolean) => void;
  [propsname: string]: any;
};

export default function Froms(props: Props) {
  const { showChildrenDrawer, item } = props;
  console.log(item);

  const [value, setValue] = useState('');
  const normFile = (e: any) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };
  const openchild = () => {
    showChildrenDrawer(true);
  };
  return (
    <div>
      <Form
        name="validate_other"
        onFinish={onFinish}
        labelCol={{ span: 2 }}
        initialValues={item && item}
      >
        <Form.Item
          label="名称"
          name="title"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="描述"
          name="summary"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <TextArea
            value={value}
            onChange={(e) => setValue(e.target.value)}
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
        <Form.Item
          name="isCommentable"
          label="评论"
          valuePropName="checked"
          colon={false}
          style={{ marginBottom: 15 }}
        >
          <Switch />
        </Form.Item>

        <Form.Item label="封面" colon={false} style={{ marginBottom: 15 }}>
          <Form.Item
            valuePropName="fileList"
            getValueFromEvent={normFile}
            noStyle
          >
            <Upload.Dragger
              name="files"
              isImageUrl={() => true}
              action="/upload.do"
            >
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">点击选择文件或将文件拖拽至此处</p>
              <p className="ant-upload-hint">
                文件将上传到OSS,如未配置请先配置
              </p>
            </Upload.Dragger>
          </Form.Item>
        </Form.Item>
        <Form.Item label=" " name="cover" colon={false} labelCol={{ span: 2 }}>
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 2 }} style={{ marginBottom: 15 }}>
          <Button htmlType="submit" onClick={openchild}>
            选择文件
          </Button>
          <Button danger style={{ marginLeft: 10 }}>
            移除
          </Button>
        </Form.Item>
      </Form>
      <div className={style.form_box}></div>
      <div className={style.btn_box}>
        <Button htmlType="submit">取消</Button>
        <Button type="primary">更新</Button>
      </div>
    </div>
  );
}
