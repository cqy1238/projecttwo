import React from 'react';
import { Form, Input, Button, Select, Row, Col } from 'antd';
import style from './index.less';
const { Option } = Select;
const App: React.FC = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.poster_box}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
      >
        <Row style={{ width: '100%', alignContent: 'center' }}>
          <Col span={7}>
            <Form.Item label="文件名称" name="name" style={{ width: '100%' }}>
              <Input placeholder="请输入页面名称" />
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
          <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button htmlType="button">重置</Button>
          </Form.Item>
        </Row>
      </Form>
    </div>
  );
};

export default App;
