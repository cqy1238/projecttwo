import React, { useState } from 'react';
import { Table, Button, Tooltip } from 'antd';
import { RedoOutlined } from '@ant-design/icons';
import type { TableRowSelection } from 'antd/lib/table/interface';
import style from './index.less';
interface Iprops {
  columns: any;
  data: any;
  keys: any;
  newCon: any;
  reload: any;
  pagination: any;
}
interface DataType {
  [propsName: string]: any;
}
const App: React.FC<Iprops> = ({
  columns,
  data,
  children,
  keys,
  newCon,
  reload,
  pagination,
}) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
    keys(newSelectedRowKeys);
  };
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <div className={style.tableWarp}>
      <div className={style.actionTop}>
        {selectedRowKeys.length > 0 ? (
          <div className={style.actionLeft}>{children}</div>
        ) : (
          <div></div>
        )}

        <div className={style.actionRight}>
          <Button type="primary" onClick={newCon}>
            + 新建
          </Button>
          <div></div>
          <Tooltip title="刷新">
            <RedoOutlined
              style={{ fontSize: 18 }}
              rotate={270}
              onClick={reload}
            />
          </Tooltip>
        </div>
      </div>

      <Table
        rowKey={(record) => record.id}
        scroll={{ x: 1500 }}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        pagination={pagination}
      />
    </div>
  );
};

export default App;
