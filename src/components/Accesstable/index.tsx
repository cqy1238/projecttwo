import React, { useEffect, useState } from 'react';
import { Table, Space, Badge, message, Popconfirm } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import style from './index.less';
import { getview, delview } from '@/services/Access';
import { useRequest } from 'umi';

import RedoOutlined from '@ant-design/icons/lib/icons/RedoOutlined';
interface Iprops {
  tablefrom: any;
}
interface DataType {
  id: string;
  key: number;
  ip: string;
  userAgent: string;
  url: string;
  count: number;
  address: string;
  browser: string;
  engine: string;
  device: string;
  os: string;
  createAt: string;
  updateAt: string;
}
const cancel = () => {
  message.error('Click on No');
};

const App: React.FC<Iprops> = ({ tablefrom }) => {
  let { run } = useRequest(delview, { manual: true });
  let res = useRequest(getview, { manual: true });
  const [length, setlength] = useState(0);
  const [lengtht, setlengtht] = useState(0);
  let [page, setpage] = useState(1);
  let [pageSize, setpagesize] = useState(12);
  let [tabledata, settabledata] = useState([]);
  let [newtabledata, newsetdata] = useState([]);
  useEffect(() => {
    let newdata = tabledata.filter(
      (
        item: {
          ip: string;
          engine: string;
          os: string;
          url: string;
          address: string;
          userAgent: string;
          browser: string;
        },
        index,
      ) => {
        return (
          (item.ip.includes(tablefrom.ip) || tablefrom.ip == undefined) &&
          (item.userAgent.includes(tablefrom.ui) ||
            tablefrom.ui == undefined) &&
          (item.address.includes(tablefrom.gps) ||
            tablefrom.gps == undefined) &&
          (item.browser.includes(tablefrom.coml) ||
            tablefrom.com == undefined) &&
          (item.engine.includes(tablefrom.cmol) ||
            tablefrom.cmol == undefined) &&
          (item.os.includes(tablefrom.os) || tablefrom.os == undefined)
        );
      },
    );
    newsetdata(newdata);
    setlength(newdata.length);
    console.log(newdata);
    if (tablefrom.del != 'true') {
      setlength(newdata.length);
    } else {
      setlength(lengtht);
    }
  }, [tablefrom]);
  const init = () => {
    res.run({
      page: page,
      pageSize: pageSize,
    });
  };
  useEffect(() => {
    init();
  }, []);
  useEffect(() => {
    if (res.data) {
      settabledata(res.data[0]);
      setlength(res.data[1]);
      newsetdata(res.data[0]);
      console.log(res.data[0]);
      setlengtht(res.data[1]);
    }
  }, [res.data]);
  const [selectedRowKeys, setSelectedRowKeys] = useState<string[]>([]);
  const onSelectChange = (newSelectedRowKeys: any[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const delserchtable = () => {
    selectedRowKeys.forEach((item, index) => {
      run(item);
    });
    init();
    message.success('Click on Yes');
  };
  const columns: ColumnsType<DataType> = [
    {
      title: 'URL',
      // dataIndex: 'url',
      fixed: 'left',
      align: 'center',

      render: (text, record) => (
        <Space size="middle">
          <a href={style.url} className={style.url}>
            {record.url}
          </a>
        </Space>
      ),
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      align: 'center',
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      align: 'center',
    },
    {
      title: '内核',
      dataIndex: 'engine',
      align: 'center',
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      align: 'center',
    },
    {
      title: '设备',
      dataIndex: '',
      align: 'center',
    },
    {
      title: '地址',
      dataIndex: 'address',
      align: 'center',
    },

    {
      title: '访问量',
      dataIndex: 'count',
      align: 'center',
      render: (text, record) => (
        <Badge
          className="site-badge-count-109"
          count={record.count}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '200px',
      render: (text, record) => (
        <Space size="middle">
          <Popconfirm
            title="Are you sure to delete this task?"
            onConfirm={() => {
              run(record.id);
              message.success('Click on Yes');
              init();
            }}
            onCancel={() => cancel()}
            okText="Yes"
            cancelText="No"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const changepage = (page: number, pageSize: number) => {
    setpage(page);
    setpagesize(pageSize);
    res.run({
      page: page,
      pageSize: pageSize,
    });
  };
  return (
    <div className={style.tabbox}>
      <div className={style.navtob}>
        <Popconfirm
          title="你确定不"
          onConfirm={() => delserchtable()}
          onCancel={() => cancel()}
          okText="确定"
          cancelText="不确定"
        >
          <button
            className={style.butdel}
            style={{ opacity: selectedRowKeys.length > 0 ? 1 : 0 }}
            onClick={() => {
              console.log(selectedRowKeys);
            }}
          >
            删除
          </button>
        </Popconfirm>

        <RedoOutlined style={{ fontSize: 16 }} rotate={270} />
      </div>
      <Table
        rowKey={(record) => record.id}
        scroll={{ x: 1500 }}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={newtabledata}
        pagination={{
          total: length,
          showTotal: (total) => `共 ${total} 条`,
          current: page,
          pageSize: pageSize,
          pageSizeOptions: [8, 12, 24, 36],
          onChange: (page, pageSize) => changepage(page, pageSize),
        }}
      />
    </div>
  );
};

export default App;
