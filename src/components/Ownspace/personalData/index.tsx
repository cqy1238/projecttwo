import React, { useState, useEffect } from 'react';
import './index.less';
import { Tabs, Button, Form, Input, Avatar, message } from 'antd';
import { useRequest, useHistory } from 'umi';
import { getUpdate, getPassword } from '@/services/user';
import Drawer from './drawer/index';
const { TabPane } = Tabs;

// tab切换
const onChange = (key: string) => {};
const PersonalData = (props: any) => {
  // const [fileItem,setFileItem] = useState({});

  let fileItem =
    localStorage.getItem('fileItem') &&
    JSON.parse(localStorage.getItem('fileItem')!);
  console.log(fileItem, 'fileItem');
  const history = useHistory();
  const [visible, setVisible] = useState(false);
  let userInfo =
    localStorage.getItem('userinfo') &&
    JSON.parse(localStorage.getItem('userinfo')!);
  const { data, run, loading } = useRequest(getUpdate, { manual: true });
  const { run: passwordRun } = useRequest(getPassword, { manual: true });
  // 修改用户名、邮箱
  const onFinish = (values: any) => {
    run({
      ...userInfo,
      name: values.name,
      email: values.email,
    }).then((res: any) => {
      console.log(res, '修改用户名');
      userInfo.name = res.name;
      userInfo.email = res.email;
      window.localStorage.setItem('userinfo', JSON.stringify(userInfo));
      window.location.reload();
      message.success('用户信息已保存');
    });
  };
  // 更新密码
  const updateOnFinish = (values: any) => {
    console.log('Success:', values);
    passwordRun({
      ...userInfo,
      oldPassword: values.oldPassword,
      newPassword: values.newPassword,
    }).then((res: any) => {
      console.log(res, 'res');

      history.push('/login');
    });
  };
  useEffect(() => {}, []);
  return (
    <div className="personalData_wrap">
      <div style={{ padding: '16px 0 16px 24px', fontSize: '16px' }}>
        个人资料
      </div>
      <div style={{ borderBottom: '1px solid #f3f3f3' }}></div>
      <div style={{ padding: '24px' }}>
        <Tabs defaultActiveKey="1" onChange={onChange}>
          {/* 基本设置 */}
          <TabPane tab="基本设置" key="1">
            <Form
              name="basic"
              labelCol={{ span: 100 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete="off"
              className="personalData-form"
              layout="inline"
            >
              <Form.Item className="personalData-item_avatar">
                <div onClick={() => setVisible(true)}>
                  <Avatar
                    className="personalData-avatar"
                    size={64}
                    src={fileItem.url}
                  ></Avatar>
                </div>
              </Form.Item>

              <Form.Item label="用户名" name="name" className="item">
                <div>
                  <Input
                    placeholder="请输入用户名"
                    defaultValue={userInfo.name}
                    style={{ width: '486px', marginLeft: '36px' }}
                  />
                </div>
              </Form.Item>
              <Form.Item label="邮箱" name="email" className="item">
                <Input
                  placeholder="请输入邮箱"
                  style={{ width: '486px', marginLeft: '50px' }}
                  defaultValue={userInfo.email}
                />
              </Form.Item>
              <Form.Item className="item">
                <Button type="primary" htmlType="submit">
                  保存
                </Button>
              </Form.Item>
            </Form>
            <Drawer visible={visible} setVisible={setVisible} />
          </TabPane>
          {/* 更新密码 */}
          <TabPane tab="更新密码" key="2">
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={updateOnFinish}
              autoComplete="off"
              style={{
                display: 'flex',
                flexDirection: 'column',
              }}
              layout="inline"
            >
              <Form.Item
                label="原密码"
                name="oldPassword"
                className="personalData-password-item"
              >
                <Input.Password
                  placeholder="请输入原密码"
                  style={{ width: '486px', marginLeft: '35px', height: '40px' }}
                />
              </Form.Item>
              <Form.Item
                label="新密码"
                name="newPassword"
                className="personalData-password-item"
              >
                <Input.Password
                  placeholder="请输入新密码"
                  style={{ width: '486px', marginLeft: '35px', height: '40px' }}
                />
              </Form.Item>
              <Form.Item
                label="确认密码"
                name="confirm"
                className="personalData-password-item"
              >
                <Input.Password
                  placeholder="请确认新密码"
                  style={{ width: '486px', marginLeft: '20px', height: '40px' }}
                />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  更新
                </Button>
              </Form.Item>
            </Form>
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};
export default PersonalData;
