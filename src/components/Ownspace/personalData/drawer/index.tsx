import { Button, Drawer } from 'antd';
import React, { useState } from 'react';
import { CloseOutlined } from '@ant-design/icons';
import FileManager from '@/pages/FileManager/index';

interface Iprops {
  visible: boolean;
  setVisible: (flag: boolean) => void;
}

const DrawerFile = (props: Iprops) => {
  const onClose = () => {
    props.setVisible(false);
  };

  const onChildrenDrawerClose = () => {
    props.setVisible(false);
  };

  return (
    <>
      <Drawer
        title="文件选择"
        width={800}
        closable={false}
        onClose={onChildrenDrawerClose}
        visible={props.visible}
        extra={<CloseOutlined onClick={onChildrenDrawerClose} />}
      >
        <FileManager type="knowledge" />
      </Drawer>
    </>
  );
};

export default DrawerFile;
