import React, { useState, useEffect } from 'react';
import style from './index.less';
import { useRequest } from 'umi';
import { getAllArticle, categoryNum, tagNum } from '@/services/article';
import { getCommentList } from '@/services/comment';
import { getFileList } from '@/services/file';
const SystemView: React.FC = () => {
  const res: any = useRequest(getAllArticle);
  const commentData: any = useRequest(getCommentList);
  const fileData: any = useRequest(getFileList);
  const categoryData: any = useRequest(categoryNum);
  console.log(categoryData);

  const tagData: any = useRequest(tagNum);
  return (
    <div className={style.systemView_wrap}>
      <div style={{ height: '47px', paddingLeft: '24px', lineHeight: '47px' }}>
        系统概览
      </div>
      <div style={{ borderBottom: '1px solid #f3f3f3' }}></div>
      <ul className={style.ul}>
        <li>累计发表了 {res.data && res.data[1]} 篇文章</li>
        <li>
          累计创建了 {categoryData.data && categoryData.data.length} 个分类
        </li>
        <li>累计创建了 {tagData.data && tagData.data.length} 个标签</li>
        <li>累计上传了 {fileData.data && fileData.data[1]} 个文件</li>
        <li>累计获得了 {commentData.data && commentData.data[1]} 个评论</li>
      </ul>
    </div>
  );
};
export default SystemView;
