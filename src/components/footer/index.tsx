import './index.less';
import { GithubOutlined } from '@ant-design/icons';
const Footer = () => {
  return (
    <div className="footer">
      <GithubOutlined style={{ fontSize: 24, color: '#535456' }} />
      <span>Copyright © 2022 Designed by Fantasticit.</span>
    </div>
  );
};

export default Footer;
