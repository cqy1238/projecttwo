import { Breadcrumb, Affix } from 'antd';
import './index.less';
const BreadcrumbCom = () => {
  return (
    <Affix offsetTop={48}>
      <Breadcrumb>
        <Breadcrumb.Item>
          <a href="">工作台</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="">所有文章</a>
        </Breadcrumb.Item>
      </Breadcrumb>
    </Affix>
  );
};

export default BreadcrumbCom;
