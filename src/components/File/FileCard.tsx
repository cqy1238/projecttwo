import React from 'react';
import style from './FileCard.less';
import moment from 'moment';
import { UploadOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { Button, message, Upload } from 'antd';
import { getOSS } from '@/services/OSS';

type Props = {
  file: any;
  showInfo?: (obj: object) => void;
  callType: string;
  reload: () => void;
};

const fileCard = (props: Props) => {
  const uprops: UploadProps = {
    name: 'file',
    multiple: true,
    customRequest: async ({ file }) => {
      const data = new FormData();
      data.append('file', file);
      let res = await getOSS(data);
      if (res.statusCode === 201) {
        message.success(`文件上传成功`);
        props.reload();
      }
    },
  };
  let { file } = props;
  return (
    <>
      {props.callType === 'knowledge' ? (
        <Upload {...uprops} showUploadList={false}>
          <Button icon={<UploadOutlined />}>上传文件</Button>
        </Upload>
      ) : null}
      <div className={style.fileCard}>
        {file &&
          file.map((item: any, index: number) => {
            return (
              <div
                className={
                  props.callType == 'knowledge'
                    ? style.KnowCardCom
                    : style.CardCom
                }
                key={index}
                onClick={() => {
                  props.showInfo && props.showInfo(item);
                }}
              >
                <div className={style.CardImg}>
                  {item.type == 'text/plain' ? null : (
                    <img src={item.url} alt={item.originalname} />
                  )}
                </div>
                <p className={style.CardTitle}>{item.originalname}</p>
                {props.callType == 'knowledge' ? null : (
                  <p className={style.CardTime}>
                    上传于{moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}
                  </p>
                )}
              </div>
            );
          })}
      </div>
    </>
  );
};

export default fileCard;
