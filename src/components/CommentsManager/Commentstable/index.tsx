import { useState, useEffect } from 'react';
import { useRequest, useDispatch } from 'umi';
import { commentmanager } from '@/services/comment';
import TablePanel from '@/components/tablePanel';
import {
  Space,
  Button,
  Popconfirm,
  Badge,
  message,
  Select,
  Row,
  Col,
  Form,
  Input,
  Popover,
  Modal,
} from 'antd';
const { TextArea } = Input;
import type { ColumnsType } from 'antd/lib/table';
import useList from '@/hooks/useList';
import moment from 'moment';
import style from './index.less';
const { Option } = Select;
interface DataType {
  [propsName: string]: any;
}
export default function Comments() {
  //表格头信息
  const columns: ColumnsType<DataType> = [
    {
      title: '状态',
      dataIndex: 'status',
      fixed: 'left',
      width: '100px',
      render: (text, record) => {
        return (
          <>
            {record.pass ? (
              <div className={style.color}>
                <Badge color="green" text="通过" />
              </div>
            ) : (
              <div className={style.color}>
                <Badge color="gold" text="未通过" />
              </div>
            )}
          </>
        );
      },
    },
    {
      title: '称呼',
      dataIndex: 'name',
      width: '180px',
    },
    {
      title: '联系方式',
      dataIndex: 'email',
      width: '180px',
    },
    {
      title: '原始内容',
      dataIndex: 'tag',
      width: '150px',
      render: (text, record) => {
        return (
          <Popover content={record.content} title="评论详情-原始内容">
            <Button type="link">查看内容</Button>
          </Popover>
        );
      },
    },
    {
      title: 'HTML内容',
      dataIndex: 'views',
      width: '150px',
      render: (text, record) => {
        return (
          <Popover
            content={
              <div dangerouslySetInnerHTML={{ __html: record.html }}></div>
            }
            title="评论详情-HTML内容"
          >
            <Button type="link">查看内容</Button>
          </Popover>
        );
      },
    },
    {
      title: '管理文章',
      dataIndex: 'likes',
      width: '100px',
      render: (text, record) => {
        return (
          <Popover
            placement="right"
            content={
              <div className={style.found_box}>
                <span>404</span>
                <span></span>
                <span>This page could not be found.</span>
              </div>
            }
            title="页面预览"
          >
            <Button type="link">文章</Button>
          </Popover>
        );
      },
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
      width: '200px',
      render: (text, record) => {
        return (
          <span>{moment(record.createAt).format('YYYY-MM-DD HH:mm:ss')}</span>
        );
      },
    },
    {
      title: '父级评论',
      dataIndex: 'parentCommentId',
      width: '100px',
      render: (text, record) => {
        return (
          <>
            {record.parentCommentId === null ? (
              '无'
            ) : (
              <div>{record.replyUserName}</div>
            )}
          </>
        );
      },
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '260px',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={() => passorRefued({ id: record.id, flag: true })}>
            通过
          </a>
          <a onClick={() => passorRefued({ id: record.id, flag: false })}>
            拒绝
          </a>
          <a onClick={() => reply(record)}>回复</a>
          <Popconfirm
            title="确定删除这个评论?"
            onConfirm={() => confirm(record.id)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  let { role, name } = JSON.parse(localStorage.getItem('userinfo') as string);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form] = Form.useForm();
  const [value, setValue] = useState('');
  const [recordlist, setrecordlist] = useState({
    content: '',
    id: '',
    name: '',
    email: '',
    createAt: '',
    hostId: '',
    parentCommentId: '',
    url: '',
  });
  const [cates, setCate] = useState([]);
  //请求页面接口
  const { run } = useRequest(commentmanager, { manual: true });
  const { list, pagination, init, search } = useList(commentmanager);
  //设置key值
  const [key, setkey] = useState<React.Key[]>([]);

  const renderView = () => {
    message.success('操作成功');
    init({ page: pagination.page, pageSize: pagination.pageSize });
  };
  //获取keys方法
  const keys = (keys: string[]) => {
    setkey(keys);
  };
  // //新建方法
  const newCon = () => {
    console.log('新建');
  };

  //通过或拒绝
  const passorRefued = async ({ id, flag }: any) => {
    await run({ data: { pass: flag }, id, method: 'PATCH' });
    renderView();
  };
  //批量通过或批量拒绝
  const AllPassorRefused = (flag: boolean) => {
    key.forEach(async (item) => {
      await run({ data: { pass: flag }, id: item, method: 'PATCH' });
      renderView();
    });
  };
  // 回复弹窗显示
  const reply = (record: any) => {
    setIsModalVisible(true);
    setrecordlist(record);
  };
  //回复确认
  const handleOk = async () => {
    await run({
      data: {
        content: value,
        createByAdmin: true,
        email: `${name}@${name}.com`,
        hostId: recordlist.hostId,
        name: name,
        parentCommentId: recordlist.parentCommentId,
        replyUserEmail: recordlist.email,
        replyUserName: name,
        url: recordlist.url,
      },
      method: 'POST',
    });
    renderView();
    setIsModalVisible(false);
  };
  //回复取消
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  //删除
  const confirm = async (id: string) => {
    await run({
      method: 'DELETE',
      id,
    });
    renderView();
  };
  //批量删除
  const alldelete = () => {
    key.forEach(async (item) => {
      await run({ id: item, method: 'DELETE' }).then(() => {
        //重新渲染数据
        renderView();
      });
    });
  };
  //取消删除
  const cancel = () => {};
  //刷新
  const reload = () => {
    renderView();
  };
  //重置
  const onReset = () => {
    form.resetFields();
  };
  //搜索
  const onFinish = async (values: any) => {
    const res = await run({
      method: 'get',
      params: {
        // page: 1,
        // pageSize: 5,
        ...values,
      },
    });
    search(res);
  };
  return (
    <div>
      <Modal
        title="回复评论"
        okText="回复"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <TextArea
          value={value}
          onChange={(e: any) => setValue(e.target.value)}
          placeholder="支持 Markdown"
          autoSize={{ minRows: 6, maxRows: 10 }}
        />
      </Modal>
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        layout="inline"
      >
        <Row style={{ width: '100%', alignContent: 'center' }}>
          <Col span={7}>
            <Form.Item label="称呼" name="name" style={{ width: '100%' }}>
              <Input placeholder="请输入称呼" />
            </Form.Item>
          </Col>
          <Col span={7}>
            <Form.Item label="Email" name="email" style={{ width: '100%' }}>
              <Input placeholder="请输入联系方式" />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="状态" name="pass">
              <Select placeholder="" allowClear>
                <Option value="1">已通过</Option>
                <Option value="0">未通过</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
          <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button htmlType="button" onClick={onReset}>
              重置
            </Button>
          </Form.Item>
        </Row>
      </Form>
      <TablePanel
        newCon={newCon}
        keys={keys}
        columns={columns}
        data={list}
        reload={reload}
        pagination={pagination}
      >
        <Button
          style={{ marginRight: '8px' }}
          onClick={() => AllPassorRefused(true)}
        >
          通过
        </Button>
        <Button
          style={{ marginRight: '8px' }}
          onClick={() => AllPassorRefused(false)}
        >
          拒绝
        </Button>
        <Popconfirm title="确认删除?" okText="确认" cancelText="取消">
          <Button style={{ marginRight: '8px' }} danger onClick={alldelete}>
            删除
          </Button>
        </Popconfirm>
      </TablePanel>
    </div>
  );
}
