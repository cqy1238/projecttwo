import React from 'react';
import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import style from './index.less';
import { getOSS } from '@/services/OSS';
const { Dragger } = Upload;

const App: React.FC = () => {
  const props: UploadProps = {
    name: 'file',
    multiple: true,
    customRequest: async ({ file }) => {
      const data = new FormData();
      data.append('file', file);
      let res = await getOSS(data);
      if (res.statusCode === 201) {
        message.success(`文件上传成功`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };
  return (
    <Dragger {...props} className={style.OssBox} showUploadList={false}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
      <p className={style.hint}>文件将上传到OSS,如未配置请先配置</p>
    </Dragger>
  );
};

export default App;
