import React from 'react';
import { Form, Input, Button, Select } from 'antd';
import style from './index.less';

interface obj {
  [name: string]: string;
}
interface props {
  serchfrom(from: obj): void;
}
const App = (props: props) => {
  const [form] = Form.useForm();

  console.log(props);

  const onFinish = (values: any) => {
    console.log('Success:', values);
    form.setFieldsValue({
      usertype: '',
      usermsg: '',
      usenum: '',
    });
    props.serchfrom(values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.bgfrom}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
      >
        <div className={style.search}>
          <Form.Item label="类型" name="usertype">
            <Input placeholder="请输入类型" value="" />
          </Form.Item>
          <Form.Item label="搜索词" name="usermsg">
            <Input placeholder="请输入搜索词" value="" />
          </Form.Item>
          <Form.Item label="搜索量" name="usenum">
            <Input placeholder="请输入搜索量" value="" />
          </Form.Item>
        </div>

        <div className={style.butom}>
          <div></div>
          <div className={style.butombt}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button
                onClick={() => {
                  props.serchfrom({
                    usertype: '',
                    usermsg: '',
                    usenum: '',
                    del: 'true',
                  });
                  form.setFieldsValue({
                    usertype: '',
                    usermsg: '',
                    usenum: '',
                  });
                }}
              >
                重置
              </Button>
            </Form.Item>
          </div>
        </div>
      </Form>
    </div>
  );
};

export default App;
