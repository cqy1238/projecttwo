import React, { useEffect, useState } from 'react';
import { Tag, Space, Divider, Radio, Empty, Tooltip, Table } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import moment from 'moment';
import { useRequest } from 'umi';
import { getEmail } from '@/services/email';
import Spin from '../../components/Spin/index';
import { RedoOutlined } from '@ant-design/icons';
const Mock = require('mockjs');
type Props = {
  searchInfo: any;
};
interface dataType {
  createAt: string;
  from: string;
  html: string;
  id: string;
  subject: string;
  text: any;
  to: string;
}
const columns = [
  {
    title: '发件人',
    dataIndex: 'from',
    key: 'from',
  },
  {
    title: '收件人',
    dataIndex: 'to',
    key: 'to',
  },
  {
    title: '主题',
    dataIndex: 'subject',
    key: 'subject',
  },
  {
    title: '发送时间',
    dataIndex: 'createAt',
    key: 'createAt',
  },
  {
    title: '操作',
    dataIndex: 'operate',
    key: 'operate',
    render: (text: any, record: any) => (
      <a
        onClick={() => {
          console.log(record);
        }}
      >
        删除
      </a>
    ),
  },
];
const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: dataType[]) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: (record: dataType) => ({
    disabled: record.id === 'Disabled User', // Column configuration not to be checked
    name: record.id,
  }),
};
const EmailTable = (props: Props) => {
  // 页码
  const [page, setPage] = useState(1);
  // 每页显示条数
  const [pageSize, setPageSize] = useState(12);
  // 邮件总数
  const [length, setLength] = useState(0);
  const [email, setEmail] = useState([]);
  // 调接口
  const { run: emailRun, loading } = useRequest(getEmail, {
    manual: true,
  });
  let { from, to, subject } = props.searchInfo;
  const emailList = (
    page?: number,
    pageSize?: number,
    from?: string,
    to?: string,
    subject?: string,
  ) => {
    emailRun({ page, pageSize, from, to, subject }).then((data) => {
      // 转换时间戳
      if (data[0].length) {
        data[0] = data[0].map((item: any) => {
          item.createAt = moment(item.createAt).format('YYYY-MM-DD HH:mm:ss');
          return item;
        });
        setEmail(data[0]);
        setLength(data[1]);
      } else {
        let mockData = Mock.mock({
          'list|100': [
            {
              'createAt|1': [
                '2021-12-24T23:27:04.528Z',
                '2022-05-06T10:16:34.934Z',
                '2022-05-07T03:55:27.126Z',
              ],
              'to|1': ['3383856807@qq.com', '12r1d12e12e@163.com'],
              html: 'html11111',
              id: '@id',
              'subject|1': ['测试', '新评论通知'],
              text: '111',
              'from|1': ['bken2016@163.com', '3383856807@qq.com'],
            },
          ],
        }).list;
        setEmail(mockData);
      }
    });
  };
  useEffect(() => {
    emailList(page, pageSize, from, to, subject);
  }, [page, pageSize, from, to, subject]);
  const [selectionType] = useState<'checkbox' | 'radio'>('checkbox');
  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        background: '#fff',
      }}
    >
      {loading ? (
        <Spin />
      ) : (
        <>
          <div
            style={{
              width: '100%',
              height: '50px',
              display: 'flex',
              alignItems: 'center',
              background: '#fff',
              position: 'relative',
            }}
          >
            <Tooltip placement="top" title={'刷新'}>
              <RedoOutlined
                size={16}
                rotate={270}
                style={{
                  position: 'absolute',
                  right: '10px',
                }}
                onClick={() => {
                  emailList(page, pageSize, from, to, subject);
                }}
              />
            </Tooltip>
          </div>
          {email.length ? (
            <Table
              rowSelection={{
                type: selectionType,
                ...rowSelection,
              }}
              rowKey={(record) => record.id}
              dataSource={email}
              columns={columns}
              pagination={{
                current: page,
                pageSize: pageSize,
                defaultPageSize: 12,
                total: length,
                showTotal: (total) => `共${total} 条`,
                onChange: (e) => {
                  setPage(e);
                },
                onShowSizeChange: (current, size) => {
                  setPageSize(size);
                },
                pageSizeOptions: [8, 12, 24, 36],
              }}
            />
          ) : (
            <Empty />
          )}
        </>
      )}
    </div>
  );
};

export default EmailTable;
