import React, { useRef } from 'react';
import { Form, Input, Button, Select, Row, Col, Tooltip } from 'antd';
const { Option } = Select;
import style from '../file/fileForm.less';
import { RedoOutlined } from '@ant-design/icons';

type Props = {
  sendSearchInfo: (obj: object) => void;
};

const EmainForm = (props: Props) => {
  let setFrom = useRef(null) as any;
  let setTo = useRef(null) as any;
  let setSubject = useRef(null) as any;
  let onFinish = () => {
    props.sendSearchInfo({
      from: setFrom.current.input.value,
      to: setTo.current.input.value,
      subject: setSubject.current.input.value,
    });
  };
  let onReset = () => {
    props.sendSearchInfo({});
  };
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      autoComplete="off"
      layout="inline"
      className={style.publicForm}
    >
      <Row style={{ width: '100%', alignContent: 'center' }}>
        <Col span={7}>
          <Form.Item label="发件人" name="publisher" style={{ width: '100%' }}>
            <Input placeholder="请输入发件人" ref={setFrom} />
          </Form.Item>
        </Col>
        <Col span={7}>
          <Form.Item label="收件人" name="getter" style={{ width: '100%' }}>
            <Input placeholder="请输入收件人" ref={setTo} />
          </Form.Item>
        </Col>
        <Col span={7}>
          <Form.Item label="主题" name="theme" style={{ width: '100%' }}>
            <Input placeholder="请输入主题" ref={setSubject} />
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ width: '100%', justifyContent: 'flex-end' }}>
        <Form.Item wrapperCol={{ offset: 11, span: 16 }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button htmlType="button" onClick={onReset}>
            重置
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
};

export default EmainForm;
