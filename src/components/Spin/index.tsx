import { Spin } from 'antd';
import React from 'react';

const App: React.FC = () => (
  <div
    style={{
      height: '400px',
      textAlign: 'center',
      paddingTop: '50px',
    }}
  >
    <Spin />
  </div>
);

export default App;
