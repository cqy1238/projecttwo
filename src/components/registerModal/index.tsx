import React, { useEffect } from 'react';
import { history } from 'umi';
import { Modal, Button, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { confirm } = Modal;

const showConfirm = () => {
  confirm({
    title: '注册成功',
    icon: <ExclamationCircleOutlined />,
    content: '是否跳转至登录？',
    onOk() {
      history.push('/login');
    },
    onCancel() {},
  });
};
const RegisterModal: React.FC = (props: any) => {
  console.log(props, 'props');
  useEffect(() => {
    showConfirm();
  }, []);
  return (
    <Space wrap>{/* <Button onClick={showConfirm}>Confirm</Button> */}</Space>
  );
};

export default RegisterModal;
