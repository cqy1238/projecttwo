import React, { useEffect, useState } from 'react';
import { Table, Space, message, Popconfirm, Badge, Tooltip } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import style from './index.less';
import { useRequest } from 'umi';
import { delserch, getserch } from '@/services/serch';
import RedoOutlined from '@ant-design/icons/lib/icons/RedoOutlined';

interface Iprops {
  tablefrom: any;
}
interface DataType {
  id: string;
  type: string;
  keyword: string;
  count: number;
  createAt: string;
  updateAt: string;
}
const confirm = () => {
  message.success('Click on Yes');
};

const App: React.FC<Iprops> = ({ tablefrom }) => {
  let res = useRequest(getserch, { manual: true });
  let { run, data } = useRequest(delserch, { manual: true });
  const [length, setlength] = useState(0);
  const [lengtht, setlengtht] = useState(0);
  let [page, setpage] = useState(1);
  let [pageSize, setpagesize] = useState(12);
  let [tabledata, setdata] = useState([]);
  let [newtabledata, newsetdata] = useState([]);
  useEffect(() => {
    let newdata = tabledata.filter(
      (item: { keyword: string; count: number; type: string }, index) => {
        return (
          (item.keyword.includes(tablefrom.usermsg) ||
            tablefrom.usermsg == undefined) &&
          (item.type.includes(tablefrom.usertype) ||
            tablefrom.usertype == undefined)
        );
      },
    );
    newsetdata(newdata);
    if (tablefrom.del != 'true') {
      setlength(newdata.length);
    } else {
      setlength(lengtht);
    }

    console.log(newdata);
  }, [tablefrom]);
  const columns: ColumnsType<DataType> = [
    {
      title: '搜索词',
      width: '10%',
      dataIndex: 'keyword',
      align: 'center',
    },
    {
      title: '搜索量',
      width: '10%',
      align: 'center',
      render: (text, record) => (
        <Badge
          className="site-badge-count-109"
          count={record.count}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '搜索时间',
      dataIndex: 'createAt',
      width: '70%',
      align: 'left',
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: '10%',
      render: (text, record) => (
        <Space size="middle">
          <Popconfirm
            title="Are you sure to delete this task?"
            onConfirm={() => {
              run(record.id);
              message.success('Click on Yes');
              res.run({
                page: page,
                pageSize: pageSize,
              });
            }}
            okText="Yes"
            cancelText="No"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  useEffect(() => {
    console.log(tablefrom);
  }, []);
  useEffect(() => {
    res.run({
      page: page,
      pageSize: pageSize,
    });
  }, []);
  useEffect(() => {
    if (res.data) {
      console.log(res.data);
      setdata(res.data[0]);
      setlength(res.data[1]);
      newsetdata(res.data[0]);
      setlengtht(res.data[1]);
    }
  }, [res.data]);
  const [selectedRowKeys, setSelectedRowKeys] = useState<string[]>([]);
  const delserchtable = () => {
    selectedRowKeys.forEach((item, index) => {
      run(item);
    });
    message.success('Click on Yes');
    res.run({
      page: page,
      pageSize: pageSize,
    });
  };
  const onSelectChange = (newSelectedRowKeys: any[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
    console.log('selectedRowKeys changed: ', selectedRowKeys);
  };
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const changepage = (page: number, pageSize: number) => {
    setpage(page);
    setpagesize(pageSize);
    res.run({
      page: page,
      pageSize: pageSize,
    });
  };
  return (
    <div className={style.tabbox}>
      <div className={style.navtob}>
        <Popconfirm
          title="Are you sure to delete this task?"
          onConfirm={() => delserchtable()}
          onCancel={() => confirm()}
          okText="Yes"
          cancelText="No"
        >
          <button
            className={style.butdel}
            style={{ opacity: selectedRowKeys.length > 0 ? 1 : 0 }}
          >
            删除
          </button>
        </Popconfirm>
        <Tooltip placement="top" title={'刷新'}>
          <RedoOutlined style={{ fontSize: 16 }} rotate={270} />
        </Tooltip>
      </div>
      <Table
        rowKey={(record) => record.id}
        scroll={{ x: 1500 }}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={newtabledata}
        pagination={{
          total: length,
          showTotal: (total) => `共 ${total} 条`,
          current: page,
          pageSize: pageSize,
          // defaultPageSize: 12,
          pageSizeOptions: [8, 12, 24, 36],
          onChange: (page, pageSize) => changepage(page, pageSize),
        }}
      />
    </div>
  );
};

export default App;
