import React, { useEffect, useRef, useState } from 'react';
import type { DrawerProps } from 'antd';
import { Drawer, Input, message, Button, Image, Popconfirm } from 'antd';
import style from './Drawer.less';
import { CloseOutlined } from '@ant-design/icons';
import { useRequest } from 'umi';
import { delFile } from '@/services/file';
type Props = {
  flag?: boolean;
  state?: any;
  changeFlag: (flag: boolean) => void;
  resetData: (flag: boolean) => void;
};
const DrawerCom = (props: Props) => {
  // 调删除接口
  let apiDelFile = useRequest(delFile, {
    manual: true,
  });
  // 确认删除
  const yesDel = (id: string) => {
    if (
      JSON.parse(localStorage.getItem('userinfo') as string).role == 'visitor'
    ) {
      message.warning('访客无权进行操作');
    } else {
      apiDelFile.run(id).then((res) => {
        if (res) {
          message.success('操作成功');
          location.reload();
        } else {
          message.error('操作失败');
        }
      });
    }
  };
  // 键盘按下esc，关闭遮罩层
  onkeyup = (e) => {
    e.key == 'Escape' ? props.changeFlag(false) : null;
  };
  onclick = (e: any) => {
    e.target.className == 'ant-drawer-mask' ? props.changeFlag(false) : null;
  };
  // 处理存储单位
  const handleUnit = () => {
    if (props.state.size >= 1000000) {
      props.state.size = (props.state.size / 1024 / 1024).toFixed(2) + 'MB';
    } else if (props.state.size <= 1000000 && props.state.size > 0) {
      props.state.size = (props.state.size / 1024).toFixed(2) + 'KB';
    } else if (props.state.size == 0) {
      props.state.size = props.state.size + 'Byte';
    }
    if (props.state.type == 'image/jpeg') {
      props.state.type = props.state.type = 'image/jpg';
    }
    if (props.state.type == 'text/plain') {
      props.state.type = props.state.type = 'txt';
    }
  };
  handleUnit();
  // 复制
  const copyThis = () => {
    let dom = document.getElementById('copyInp') as HTMLInputElement;
    dom.select();
    document.execCommand('copy');
    message.success('内容已复制到剪贴板');
  };
  let { filename, url, originalname, id, type, size } = props.state;
  return (
    <>
      <Drawer
        title="文件信息"
        placement="right"
        closable={false}
        className={style.drawer}
        width={600}
        extra={
          <CloseOutlined
            onClick={() => {
              props.changeFlag && props.changeFlag(false);
            }}
          />
        }
        footer={
          <div className={style.drawer_footer}>
            <Button onClick={() => props.changeFlag(false)}>关闭</Button>
            <Button danger>
              <Popconfirm
                title="确认删除这个文件？"
                okText="确认"
                cancelText="取消"
                onCancel={() => {
                  console.log('取消');
                }}
                onConfirm={() => {
                  yesDel(id);
                  props.resetData(true);
                  props.changeFlag(false);
                }}
              >
                删除
              </Popconfirm>
            </Button>
          </div>
        }
        visible={props.flag}
        key={id}
      >
        <div className={style.drawerImg}>
          <img src={url} alt={originalname} className={style.image} />
          {/* <Image src={url} alt={originalname} className={style.image} /> */}
        </div>
        <p>文件名称：{originalname}</p>
        <p>存储路径：{filename}</p>
        <p>
          <span>文件类型：{type}</span>
          <span>文件大小：{size}</span>
        </p>
        <p>
          访问链接：
          <Input
            value={url}
            width={50}
            id="copyInp"
            className={style.cpInput}
            style={{ display: 'inline-block' }}
            // 点击链接进行复制
            onClick={() => copyThis()}
          />
        </p>
        <a onClick={() => copyThis()}>复制</a>
      </Drawer>
    </>
  );
};

export default DrawerCom;
